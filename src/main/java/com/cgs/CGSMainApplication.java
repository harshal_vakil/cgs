package com.cgs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@ComponentScan(basePackages = { "com.cgs" })
public class CGSMainApplication {

	public static void main(String[] args) {
		log.info("Starting Application");
		SpringApplication.run(CGSMainApplication.class, args);
		log.info("Application Started");
	}

}