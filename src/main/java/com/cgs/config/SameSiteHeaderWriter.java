package com.cgs.config;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.security.web.header.HeaderWriter;

import lombok.var;
import lombok.extern.slf4j.Slf4j;

/**
 * This header writer just adds "SameSite=lax;" to the Set-Cookie response
 * header
 */
@Slf4j
public class SameSiteHeaderWriter implements HeaderWriter {

	private static final String SAME_SITE_LAX = "SameSite=Lax";

	private static final String SECURE = "Secure";

	@Override
	public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {

		log.info("before response.containsHeader(HttpHeaders.SET_COOKIE)");
		if (response.containsHeader(HttpHeaders.SET_COOKIE)) {
			log.info("in response.containsHeader(HttpHeaders.SET_COOKIE)");
			var setCookie = response.getHeader(HttpHeaders.SET_COOKIE);
			var toAdd = new ArrayList<String>();
			toAdd.add(setCookie);

			if (!setCookie.contains(SAME_SITE_LAX)) {
				log.info("in !setCookie.contains(SAME_SITE_LAX)");
				toAdd.add(SAME_SITE_LAX);
			}

			if (!setCookie.contains(SECURE)) {
				log.info("in !setCookie.contains(SECURE)");
				toAdd.add(SECURE);
			}

			response.setHeader(HttpHeaders.SET_COOKIE, String.join("; ", toAdd));
			log.info("toAdd = " + String.join("; ", toAdd));

		}
	}

}