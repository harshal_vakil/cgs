package com.cgs.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Enable CORS and disable CSRF
		// Set session management to stateless
		http.cors().and().csrf().disable().authorizeRequests().anyRequest().permitAll().antMatchers("/login?error")
				.permitAll().and().formLogin().disable() // <-- disable spring default login route
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.headers().xssProtection().and().contentSecurityPolicy(
				"connect-src 'self' https://www.google-analytics.com/ http://caredirectory.caregiversaathi.co.in/ https://stats.g.doubleclick.net/; worker-src 'self'; prefetch-src 'self'; manifest-src 'self'; font-src 'self'; media-src 'self'; object-src 'self'; script-src 'nonce-rAnd0m'; img-src 'self'  https://www.google-analytics.com/; frame-src 'self'; style-src 'self' 'nonce-raNd0m'; form-action 'self' http://caredirectory.caregiversaathi.co.in/; frame-ancestors 'self'; report-uri /report; report-to csp-violation-report")
				.and().defaultsDisabled().contentTypeOptions().and()// .addHeaderWriter(new SameSiteHeaderWriter())
				.frameOptions().disable();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/webjars/**").antMatchers("stats.g.doubleclick.net")
				.antMatchers("google-analytics.com");
	}

}
