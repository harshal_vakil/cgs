package com.cgs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class AdminApprovalController extends BaseController {

	@PostMapping("/admin/editUserData")
	public String editUserData(Model model, HttpServletRequest request) {
		// List<Map<String, Object>> userList = userRepository.getAllUsers();
		// model.addAttribute("users", userRepository.getAllUsers());
		model.addAttribute("active_admin_sidebar", "userApprovals");
		Map<String, Object> params = new HashMap<>();
		params.put("email_id", request.getParameter("email_id"));
		params.put("hidden_email_id", request.getParameter("hidden_email_id"));
		params.put("is_approved", "Y");
		log.info("Email: " + request.getParameter("email_id"));
		log.info("Hidden Email: " + request.getParameter("hidden_email_id"));
		adminRepository.approveUser(params);
		return geRegApproval(model);
	}

	@GetMapping("/admin/registrationApproval")
	public String geRegApproval(Model model) {
		model.addAttribute("active_admin_sidebar", "userApprovals");
		int i = 1;
		List<Map<String, Object>> userList = userRepository.getAllUnapprovedUsers();
		for (Map<String, Object> user : userList) {
			user.put("userId", i++);
		}
		model.addAttribute("users", userList);
		return "admin/registrationApproval";
	}

}
