package com.cgs.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class AdminDiseasesController extends BaseController {

	@GetMapping("/admin/disease/list")
	public String adminDiseasesList(Model model) {
		populateModel(model);
		model.addAttribute("page", "diseases_list");
		model.addAttribute("diseases_list", diseaseRepository.getAllDiseases());
		return "admin/diseases";
	}

	@GetMapping("/admin/disease/new")
	public String get(Model model) {
		populateModel(model);
		model.addAttribute("page", "disease_details");
		return "admin/diseases";
	}

	public void populateModel(Model model) {
		model.addAttribute("active_admin_sidebar", "disease");
	}

	@PostMapping("/admin/disease/add")
	public String post(HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, String[]> attributes = request.getParameterMap();

		for (Entry<String, String[]> entry : attributes.entrySet()) {
			log.debug(entry.getKey() + " = " + Arrays.asList(entry.getValue()));
		}
		String diseaseName = (attributes.get("diseaseName"))[0];
		try {
			if (StringUtils.isBlank(diseaseName)) {
				model.addAttribute("error", "Disease Name cannot be blank!");
				throw new Exception("Disease Name cannot be blank!");
			}
			boolean diseaseAdded = adminRepository.addNewDisease(attributes);
			if (!diseaseAdded) {
				throw new DuplicateKeyException("Disease already exists.");
			} else {
				model.addAttribute("page", "disease_details");
				populateModel(model);
				model.addAttribute("success", "Disease: '" + diseaseName + "' Created!");
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} catch (DuplicateKeyException duplicateKeyException) {
			log.info("caught duplicate exception");
			model.addAttribute("page", "disease_details");
			populateModel(model);
			model.addAttribute("error", "Disease already exists.");
			return "admin/diseases";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return "admin/diseases";
	}

	@RequestMapping("/admin/disease/toggle")
	public String toggleDiseases(@RequestParam("toggleValue") String toggleValue,
			@RequestParam("diseaseId") String diseaseId, Model model) {
		populateModel(model);
		log.debug("Toggle Value: " + toggleValue);
		log.debug("Disease Id: " + diseaseId);
		model.addAttribute("page", "diseases_list");
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("toggle_string", toggleValue);
		parameter.put("disease_id", diseaseId);
		adminRepository.toggleDisease(parameter);
		return "admin/diseases";
	}

	@GetMapping("/admin/disease/{diseaseId}")
	public String viewDisease(Model model, @PathVariable String diseaseId) {
		populateModel(model);
		model.addAttribute("page", "disease_details");
		return "admin/diseases";
	}

}
