package com.cgs.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class AdminServicesController extends BaseController {

	@GetMapping("/admin/service/list")
	public String adminServicesList(Model model) {
		populateModel(model);
		model.addAttribute("page", "services_list");
		model.addAttribute("services_list", servicesRepository.getAllServices());
		return "admin/services";
	}

	@GetMapping("/admin/service/new")
	public String get(Model model) {
		populateModel(model);
		model.addAttribute("page", "service_details");
		model.addAttribute("disease_list", diseaseRepository.getAllEnabledDiseases());
		return "admin/services";
	}

	public void populateModel(Model model) {
		model.addAttribute("active_admin_sidebar", "service");
	}

	@PostMapping("/admin/service/add")
	public String post(HttpServletRequest request, HttpServletResponse response, Model model) {
		String serviceName = "";
		try {
			Map<String, String[]> attributes = request.getParameterMap();
			for (Entry<String, String[]> entry : attributes.entrySet()) {
				log.debug(entry.getKey() + " = " + Arrays.asList(entry.getValue()));
			}
			populateModel(model);
			model.addAttribute("page", "service_details");
			model.addAttribute("disease_list", diseaseRepository.getAllEnabledDiseases());
			serviceName = (attributes.get("serviceName"))[0];
			if (StringUtils.isBlank(serviceName)) {
				model.addAttribute("error", "Service Name cannot be blank!");
				throw new Exception("Service Name cannot be blank!");
			}
			adminRepository.addNewService(attributes);
			model.addAttribute("success", "Service '" + serviceName + "' Created!");
		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("error", "Service already Exists!");
			return "admin/services";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return "admin/services";
	}

	@RequestMapping("/admin/service/toggle")
	public String toggleServices(@RequestParam("toggleValue") String toggleValue,
			@RequestParam("serviceId") String serviceId, Model model) {
		populateModel(model);
		log.debug("Toggle Value: " + toggleValue);
		log.debug("Service Id: " + serviceId);
		model.addAttribute("page", "services_list");
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("toggle_string", toggleValue);
		parameter.put("service_id", serviceId);
		adminRepository.toggleService(parameter);
		return "admin/services";
	}

	@GetMapping("/admin/service/{serviceId}")
	public String viewService(Model model, @PathVariable String serviceId) {
		populateModel(model);
		model.addAttribute("page", "service_details");
		model.addAttribute("disease_list", diseaseRepository.getAllEnabledDiseases());
		return "admin/services";
	}

	@GetMapping("/admin/volunteer-performance")
	public String volunteerPerformanceList(Model model) {
		populateModel(model);
		model.addAttribute("page", "volunteer_performance");
		model.addAttribute("volunteer_performance", performanceRepository.getVolunteerPerformance());
		model.addAttribute("active_admin_sidebar", "performance");
		log.info(String.valueOf(performanceRepository.getVolunteerPerformance()));
		return "admin/volunteer-performance";
	}

}
