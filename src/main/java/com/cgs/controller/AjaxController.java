package com.cgs.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class AjaxController extends BaseController {

	@PostMapping("/ajax/getAllEnabledDiseases")
	@ResponseBody
	public String getAllEnabledDiseases() {
		String json = cgsUtil.convertObjectToJson(diseaseRepository.getAllEnabledDiseases());
		return json;
	}

	@PostMapping("/ajax/getAllLanguages")
	@ResponseBody
	public String getAllLanguages() {
		String json = cgsUtil.convertObjectToJson(userRepository.getAllLanguages());
		return json;
	}

	@PostMapping("/ajax/getAllStates")
	@ResponseBody
	public String getAllStates() {
		String json = cgsUtil.convertObjectToJson(citiesRepository.getAllStates());
		return json;
	}

	@PostMapping("/ajax/getExisitingVendorsDetails")
	@ResponseBody
	public String getExisitingVendorsDetails(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("vendor_id", request.getParameter("vendor_id"));
		String json = cgsUtil.convertObjectToJson(leadsRepository.getExisitingVendorsDetails(params));
		log.info("vendor data: " + json);
		return json;
	}

	@PostMapping("/ajax/getExistingVendors")
	@ResponseBody
	public String getExistingVendors(HttpServletRequest request) {
		log.info("called getExistingVendors");
		log.info("city_id: " + request.getParameter("city_id"));
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("city_id", request.getParameter("city_id"));
		String json = cgsUtil.convertObjectToJson(leadsRepository.getExistingVendors(params));
		log.info("json : " + json);
		return json;
	}

}
