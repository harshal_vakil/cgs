package com.cgs.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.cgs.repository.AdminRepository;
import com.cgs.repository.CitiesRepository;
import com.cgs.repository.DiseaseRepository;
import com.cgs.repository.LeadsRepository;
import com.cgs.repository.PerformanceRepository;
import com.cgs.repository.ServicesRepository;
import com.cgs.repository.UserRepository;
import com.cgs.repository.WorkRepository;
import com.cgs.util.CGSUtil;
import com.cgs.util.EmailUtil;
import com.cgs.util.PasswordUtil;

public abstract class BaseController {

	@Autowired
	PasswordUtil passwordUtil;

	@Autowired
	CGSUtil cgsUtil;

	@Autowired
	EmailUtil emailUtil;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ServicesRepository servicesRepository;

	@Autowired
	DiseaseRepository diseaseRepository;

	@Autowired
	AdminRepository adminRepository;

	@Autowired
	WorkRepository workRepository;

	@Autowired
	CitiesRepository citiesRepository;

	@Autowired
	LeadsRepository leadsRepository;

	@Autowired
	PerformanceRepository performanceRepository;
}
