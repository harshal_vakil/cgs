package com.cgs.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class CitiesController extends BaseController {

	@PostMapping("/cities/getCitiesByState")
	@ResponseBody
	public String getCitiesByState(@RequestParam("state") String state) {
		log.debug("State: " + state);
		Map<String, Object> params = new HashMap<>();
		params.put("state", String.valueOf(state));
		String json = cgsUtil.convertObjectToJson(citiesRepository.getAllCitiesbyStates(params));
		// log.info("Cities json: " + json);
		return json;
	}
}
