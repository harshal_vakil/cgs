package com.cgs.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class IndexController extends BaseController {

	@GetMapping("/")
	public String index(HttpServletRequest request, HttpServletResponse response, Model model) {
		log.info("Loading Index page");
		HttpSession session = request.getSession();
		// Harcoding volunteerId session right now. Later will replaced with actual
		// value
		/*
		 * log.info("Setting session value"); session.setAttribute("userId",
		 * "priyamvora99"); if (null == session.getAttribute("version")) {
		 * session.setAttribute("version", new Date().getTime()); }
		 */
		session.setAttribute("version", new Date().getTime());
		model.addAttribute("services", servicesRepository.getAllEnabledServices());
		return "welcome";
	}

}