package com.cgs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LeadsApprovalsController extends BaseController {

	@PostMapping("/admin/approvals/approveReject")
	public String approveReject(Model model, HttpServletRequest request) {
		Map<String, Object> params = new HashMap<>();
		String vendorName = request.getParameter("vendor-name");
		String volunteerId = request.getParameter("volunteer-id");
		String contact = request.getParameter("vendor-contact");
		String address = request.getParameter("vendor-address");
		String vendorId = request.getParameter("vendor-id");
		String pincode = request.getParameter("vendor-pincode");
		String city = request.getParameter("vendor-city");
		String state = request.getParameter("vendor-state");
		String serviceId = request.getParameter("service-id");
		String diseaseId = request.getParameter("disease-id");
		String serviceSubAttribute = request.getParameter("service-sub-attribute-id");
		String stock = request.getParameter("service-stock");
		String volunteerRemarks = request.getParameter("volunteer-remarks");
		String adminRemarks = request.getParameter("admin-remarks");
		String button = request.getParameter("action");
		String unverifiedLeadsId = request.getParameter("unverified_leads_id");
		String cityId = request.getParameter("city_id");
		String adminEmail = request.getParameter("admin_email");
		if (button.equals("Approve")) {
			params.put("button", "approve");
		}
		if (button.equals("Decline")) {
			params.put("button", "decline");
		}
		params.put("city_id", cityId);
		params.put("admin_email", adminEmail);
		params.put("unverified_leads_id", unverifiedLeadsId);
		params.put("vendor_name", vendorName);
		params.put("volunteer_id", volunteerId);
		params.put("vendor_contact", contact);
		params.put("vendor_address", address);
		params.put("vendor_pincode", pincode);
		params.put("vendor_city", city);
		params.put("vendor_state", state);
		params.put("service_id", serviceId);
		params.put("disease_id", diseaseId);

		params.put("service_sub_attribute_id", serviceSubAttribute);
		params.put("stock", stock);
		params.put("volunteer_remarks", volunteerRemarks);
		params.put("admin_remarks", adminRemarks);
		params.put("vendor_id", vendorId);
		if (params.get("admin_remarks") == "") {
			params.put("admin_remarks", null);
		}
		if (params.get("volunteer_remarks") == "") {
			params.put("volunteer_remarks", null);
		}
		log.info("Approve-Reject params: " + params);
//		model.addAttribute("page", "admin_approval_details");
		adminRepository.approveRejectLead(params);
		return viewApprovals(model);
	}

	@GetMapping("/admin/approvals/{unverifiedLeadsId}")
	public String viewApprovalDetails(Model model, @PathVariable String unverifiedLeadsId) {
		Map<String, Object> params = new HashMap<>();
		params.put("unverified_leads_id", unverifiedLeadsId);
		Map<String, Object> approvalDetails = adminRepository.getApprovalDetail(params);
		log.info("approvalDeatils: " + approvalDetails);
		model.addAttribute("approval_details", approvalDetails);
		model.addAttribute("page", "admin_approval_details");
		return "admin/leadsApproval";
	}

	@GetMapping("/admin/leadsApprovals")
	public String viewApprovals(Model model) {
		List<Map<String, Object>> approvalsList = adminRepository.getApprovals();
		model.addAttribute("approvals", approvalsList);
		model.addAttribute("page", "admin_approvals");
		model.addAttribute("active_admin_sidebar", "approvals");
		log.info("approvalsList: " + approvalsList);
		return "admin/leadsApproval";
	}
}
