package com.cgs.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LeadsController extends BaseController {

	@PostMapping("/editLeads")
	public String loadEditLeads(Model model, HttpServletRequest request) {
		String emailId = request.getParameter("email_id");
		String vendorId = request.getParameter("vendor_id");
		String serviceId = request.getParameter("service_id");
		String diseaseId = request.getParameter("disease_id");
		log.info("emailId = " + emailId);
		log.info("vendorId = " + vendorId);
		log.info("serviceId = " + serviceId);
		log.info("diseaseId = " + diseaseId);
		Map<String, Object> vendor = leadsRepository.getVendorById(vendorId);
		log.info("vendor = " + vendor);
		model.addAttribute("vendor", vendor);
		model.addAttribute("vendorId", vendorId);
		model.addAttribute("services", servicesRepository.getAllEnabledServices());
		model.addAttribute("diseases", diseaseRepository.getAllEnabledDiseases());
		model.addAttribute("states", citiesRepository.getAllStates());

		Map<String, String> params = new HashMap<>();
		params.put("vendorId", vendorId);
		params.put("serviceId", serviceId);
		params.put("diseaseId", diseaseId);
		model.addAttribute("pageData", params);

		return "leads";
	}

	@GetMapping("/leads")
	public String loadLeads(Model model) {
		model.addAttribute("services", servicesRepository.getAllEnabledServices());
		model.addAttribute("diseases", diseaseRepository.getAllEnabledDiseases());
		model.addAttribute("states", citiesRepository.getAllStates());
		log.info("Vendor Details");
		log.info("states: " + citiesRepository.getAllStates());
		return "leads";
	}

	@PostMapping("/leads/submit")
	@ResponseBody
	public String registerVendor(@RequestParam("volunteerId") String volunteerId,
			@RequestParam("vendorId") String vendorId, @RequestParam("vendorName") String vendorName,
			@RequestParam("vendorAddress") String vendorAddress, @RequestParam("contactNumber") String contactNumber,
			@RequestParam("state") String state, @RequestParam("city") String city,
			@RequestParam("pincode") String pincode, @RequestParam("serviceType") String serviceType,
			@RequestParam("disease") String disease,
			@RequestParam("serviceAttributeId[]") ArrayList<String> serviceAttributeId,
			@RequestParam("stock[]") ArrayList<String> stock, @RequestParam("remarks") String remarks,
			@RequestParam("lastVerifiedDate") String lastVerifiedDate, Model model) {
		log.info("Service type: " + serviceType);
		HashMap<String, Object> params = new HashMap<>();
		params.put("volunteer_id", volunteerId);
		params.put("vendor_name", vendorName);
		params.put("vendor_address", vendorAddress);
		params.put("contact_number", contactNumber);
		params.put("state", state);
		params.put("city", city);
		params.put("pincode", pincode);
		params.put("service_type", serviceType);
		params.put("disease", disease);
		if (serviceAttributeId.size() == 1 && serviceAttributeId.get(0).equals("No service attribute")) {
			params.put("service_attribute_id", null);
			params.put("stock", null);
		} else {
			params.put("service_attribute_id", serviceAttributeId);
			params.put("stock", stock);
		}
		params.put("last_verified_date", lastVerifiedDate);
		params.put("remarks", remarks);

		log.info("vendorId: " + vendorId + " volunteerId: " + volunteerId + " city: " + city + " serviceType: "
				+ serviceType + " disease: " + disease + " serviceAttributeId: " + serviceAttributeId.toString()
				+ " stock: " + stock.toString());
		if (vendorId == "" || vendorId == null) {
			log.info("Vendor id is null");
			params.put("vendor_id", null);
			// add a new vendor and add in unverified leads
		} else {
			params.put("vendor_id", vendorId);
			// add in unverified leads.
		}
		return leadsRepository.createLead(params);
	}
}
