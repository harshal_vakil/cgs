package com.cgs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cgs.model.Email;
import com.cgs.model.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LoginController extends BaseController {

	@Value("${spring.mail.admin}")
	String admin;

	private void emailAdmin(Map<String, Object> params) {
		Email email = new Email();
		email.setEmailSubject("Caregiver Sathi - User Approval Required");
		email.setEmailTo(admin);
		StringBuilder emailBody = new StringBuilder();
		emailBody.append("<html>\n" + "<body>\n" + "<h3> New User Approval</h3>\n" + "<p> Name: "
				+ params.get("first_name") + " " + params.get("last_name") + "</p>\n" + "<p> Yours truly,</p>\n"
				+ "<p> CaregiverSaathi Team </p>\n" + "</body>\n" + "</html>");
		email.setEmailBody(emailBody.toString());
		emailUtil.sendMail(email);
	}

	@GetMapping("/login")
	public String loadLogin(Model model) {
		log.info("Loading Login page");
		// model.addAttribute("userModel", new User());
		return "login"; // view
	}

	@GetMapping("/register")
	public String loadRegister(Model model) {
		log.info("Loading Registration page");
		model.addAttribute("roles", userRepository.getAllRoles());
		return "register";
	}

	@PostMapping(value = "/login")
	public String login(@Valid @ModelAttribute("userModel") User userModel, HttpSession httpSession, Model model)
			throws Exception {
		httpSession.setAttribute("version", new Date().getTime());
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		params.put("email_id", userModel.getEmailId().toLowerCase());
		List<Map<String, Object>> result = new ArrayList<>();
		params.put("password", passwordUtil.encrypt(userModel.getPassword()));
		try {
			result = userRepository.findUserbyEmailPassword(params);
			if (!result.isEmpty()) {
				resultMap = result.get(0);
				httpSession.setAttribute("userInfo", resultMap);
				model.addAttribute("services", servicesRepository.getAllEnabledServices());
				return "welcome";
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("error",
				"Either your account is not approved by admin or username/password is incorrect. Please contact administration or recheck credentials");
		return "login";
	}

	@PostMapping(value = "/register")
	public String registerUser(@ModelAttribute("userModel") User user, Model model) {
		Map<String, Object> params = new HashMap<>();
		try {
			if (!user.getPassword().equals(user.getConfirmPassword())) {
				model.addAttribute("error", "Passwords do not match. Please check again.");
			} else {
				params.put("email_id", user.getEmailId().toLowerCase());
				params.put("password", passwordUtil.encrypt(user.getPassword()));
				params.put("first_name", user.getFirstName());
				params.put("last_name", user.getLastName());
				params.put("contact_no", user.getContactNo());
				params.put("role_id", "2");
				params.put("age", user.getAge());
				params.put("city_id", user.getCityId());
				params.put("create_ui", user.getEmailId().toLowerCase());
				params.put("time_contribution_frequency", user.getTimeContributionFrequency());
				params.put("time_contribution_hours", user.getTimeContributionHours());
				params.put("orientation", user.getOrientation());
				String languageId = "";
				if (null != user.getLanguageId())
					for (int i = 0; i < user.getLanguageId().size(); i++) {
						if (i == user.getLanguageId().size() - 1) {
							languageId += user.getLanguageId().get(i);
						} else {
							languageId += user.getLanguageId().get(i) + ",";
						}
					}
				params.put("language_id", languageId);
				params.put("create_ts", new Date());
				log.info("Language id: " + params.get("language_id"));

				userRepository.register(params);
				model.addAttribute("success", "Registration Successful");
				emailAdmin(params);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("error", "There has been an error while registration");
		}
		return "register";
	}

	@PostMapping("/sendPasswordMail")
	public String sendPasswordMail(@RequestParam(name = "emailId") String emailId, Model model) {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Email email = new Email();
		params.put("email_id", emailId.toLowerCase());
		List<Map<String, Object>> result = new ArrayList<>();
		try {
			result = userRepository.findUserbyEmail(params);
			if (!resultMap.isEmpty()) {
				resultMap = result.get(0);
			} else {
				model.addAttribute("error", "Email provided is not registered with us.");
				return "login";
			}

			String encryptedPassword = resultMap.get("password").toString();
			String decryptedPassword = passwordUtil.decrypt(encryptedPassword);
			setForgotPasswordEmail(emailId.toLowerCase(), decryptedPassword, email);
			emailUtil.sendMail(email);
			model.addAttribute("success", "Mail sent with password. Please check your inbox.");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("error", "There was an error processing this request.");
			return "login";
		}
		return "login";
	}

	private Email setForgotPasswordEmail(String to, String decryptedPassword, Email email) {
		email.setEmailTo(to);
		email.setEmailSubject("Forgot Your Password? Here's your old password.");
		StringBuilder emailBody = new StringBuilder();
		emailBody.append("<html>\n" + "<body>\n" + "<p> Greetings from CaregiverSaathi Foundation,</p>\n"
				+ "<p> We are delighted with your return</p>\n" + "<p> Here's the password you misplaced - </p>\n"
				+ "\n" + "<p> Old password : " + decryptedPassword + "</p>\n" + "<p> Yours truly,</p>\n"
				+ "<p> CaregiverSaathi Team </p>\n" + "</body>\n" + "</html>");
		email.setEmailBody(emailBody.toString());
		return email;
	}

}
