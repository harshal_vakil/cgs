package com.cgs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LogoutController extends BaseController {

	@GetMapping("/logout")
	public String logout(Model model, HttpSession session) {
		log.info("Logging out");
		session.invalidate();
		return "login"; // view
	}
}
