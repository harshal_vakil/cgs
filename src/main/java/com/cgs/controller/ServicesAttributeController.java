package com.cgs.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cgs.repository.ServicesAttributeRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ServicesAttributeController extends BaseController {

	@Autowired
	ServicesAttributeRepository servicesAttributeRepository;

	@PostMapping("/leads/edit/serviceAttributes")
	@ResponseBody
	public String getEditServiceAttributes(@RequestParam("serviceId") String serviceId,
			@RequestParam("vendorId") String vendorId, @RequestParam("diseaseId") String diseaseId) {
		log.info("serviceId " + serviceId + " vendorId: " + vendorId + " diseaseId: " + diseaseId);
		Map<String, Object> params = new HashMap<>();
		params.put("service_id", serviceId);
		params.put("vendor_id", vendorId);
		params.put("disease_id", diseaseId);
		String json = cgsUtil.convertObjectToJson(servicesAttributeRepository.getEditServiceAttributes(params));
		return json;

	}

	@PostMapping("/leads/service/serviceAttributes")
	@ResponseBody
	public String getServiceSubAttributes(@RequestParam("serviceId") String serviceId) {
		log.info("Service id: " + serviceId);
		Map<String, Object> params = new HashMap<>();
		params.put("service_id", serviceId);
		String json = cgsUtil.convertObjectToJson(servicesAttributeRepository.getServiceAttributes(params));
		log.info("Service attributes: " + json);
		return json;
	}
}
