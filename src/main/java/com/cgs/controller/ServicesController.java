package com.cgs.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@SuppressWarnings("unchecked")
public class ServicesController extends BaseController {

	@PostMapping("/getFilteredServiceVendors")
	public String getFilteredServiceVendors(HttpServletRequest request, Model model) {
		populateModel(model);

		Map<String, String[]> attributes = request.getParameterMap();
		for (Entry<String, String[]> entry : attributes.entrySet()) {
			log.debug(entry.getKey() + " = " + Arrays.asList(entry.getValue()));
		}

		Map<String, Object> params = new HashMap<>();

		String serviceId = sanitizeInput(request.getParameter("service_id"));
		params.put("service_id", serviceId);

		String cityId = sanitizeInput(request.getParameter("city_id"));
		if (StringUtils.isBlank(cityId))
			cityId = null;
		params.put("city_id", cityId);

		String diseaseId = sanitizeInput(request.getParameter("disease_id"));
		if (StringUtils.isBlank(diseaseId))
			diseaseId = null;
		params.put("disease_id", diseaseId);

		String pincode = sanitizeInput(request.getParameter("pincode"));
		if (StringUtils.isBlank(pincode))
			pincode = null;
		params.put("pincode", pincode);

		String state = sanitizeInput(request.getParameter("states"));
		if (StringUtils.isBlank(state))
			state = null;
		params.put("state", state);

		List<Map<String, Object>> vendorList = servicesRepository.getFilteredServiceVendors(params);
		Map<String, Object> serviceDetails = servicesRepository.getServiceDetails(params);

		serviceDetails.put("disease_id", diseaseId);
		model.addAttribute("service_details", serviceDetails);
		model.addAttribute("vendors_list", vendorList);
		model.addAttribute("page", "service_list");
		model.addAttribute("pageData", params);
		log.info("params = " + params);

		HttpSession session = request.getSession();
		session.setAttribute("search", params);

		return "services";
	}

	// public String sanitizeInput(String value) {
	// return "";
	// }

	public void populateModel(Model model) {
		List<Map<String, Object>> services = servicesRepository.getAllEnabledServices();
		model.addAttribute("services", services);
	}

	public String sanitizeInput(String value) {
		// You'll need to remove the spaces from the html entities below
		// logger.info("In cleanXSS RequestWrapper ..............." + value);
		if (value == null)
			return "";
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		value = value.replaceAll("'", "& #39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");

		value = value.replaceAll("(?i)<script.*?>.*?<script.*?>", "");
		value = value.replaceAll("(?i)<script.*?>.*?</script.*?>", "");
		value = value.replaceAll("(?i)<.*?javascript:.*?>.*?</.*?>", "");
		value = value.replaceAll("(?i)<.*?\\s+on.*?>.*?</.*?>", "");
		value = value.replaceAll("<script>", "");
		value = value.replaceAll("</script>", "");
		// logger.info("Out cleanXSS RequestWrapper ........ value ......." + value);
		return value;
	}

	@GetMapping("/service/{serviceId}")
	public String serviceList(HttpServletRequest request, Model model, @PathVariable String serviceId) {
		log.info("Loading serviceList: " + serviceId);
		populateModel(model);
		Map<String, Object> params = new HashMap<>();
		params.put("service_id", serviceId);
		params.put("disease_id", "1");
		List<Map<String, Object>> vendorList = servicesRepository.getServiceVendors(params);
		Map<String, Object> serviceDetails = servicesRepository.getServiceDetails(params);
		serviceDetails.put("disease_id", "1");
		model.addAttribute("service_details", serviceDetails);
		model.addAttribute("vendors_list", vendorList);
		model.addAttribute("page", "service_list");
		model.addAttribute("pageData", params);

		HttpSession session = request.getSession();
		session.setAttribute("search", new HashMap<String, Object>());

		return "services";
	}

	@GetMapping("/service/vendor/{vendorId}/{serviceId}/{diseaseId}")
	public String serviceListDetails(HttpServletRequest request, Model model, @PathVariable String vendorId,
			@PathVariable String serviceId, @PathVariable String diseaseId) {
		populateModel(model);
		log.info("Loading Service List Details");
		if (StringUtils.isBlank(diseaseId) || diseaseId.equalsIgnoreCase("0"))
			diseaseId = "1";
		log.info("vendorId = " + vendorId);
		log.info("serviceId = " + serviceId);
		log.info("diseaseId = " + diseaseId);
		Map<String, Object> params = new HashMap<>();
		params.put("vendor_id", vendorId);
		params.put("service_id", serviceId);
		params.put("disease_id", diseaseId);
		Map<String, Object> serviceDetails = servicesRepository.getServiceDetails(params);
		model.addAttribute("service_details", serviceDetails);
		Map<String, Object> vendorDetails = servicesRepository.getServiceVendorDetails(params);
		model.addAttribute("vendor_details", vendorDetails);
		model.addAttribute("page", "vendor_details");

		HttpSession session = request.getSession();
		Map<String, Object> tempParams = (Map<String, Object>) session.getAttribute("search");
		if (tempParams != null)
			params.putAll(tempParams);
		model.addAttribute("pageData", params);
		return "services";
	}

}