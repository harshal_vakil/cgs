package com.cgs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class StaticPagesController extends BaseController {

	@GetMapping("/contact-us")
	public String loadContactUs(Model model) {
		log.info("Loading contactUs page");
		return "contact-us"; // view
	}

	@GetMapping("/disclaimer")
	public String loadDisclaimer(Model model) {
		log.info("Loading disclaimer page");
		return "disclaimer"; // view
	}
}
