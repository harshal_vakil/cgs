package com.cgs.controller;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class VolunteerController extends BaseController {

	@GetMapping("/volunteer/register")
	public String loadRegister(Model model) {
		model.addAttribute("page", "volunteer_details");
		// registerVolunteer(model);
		return "volunteer-registration";
	}

	@PostMapping("/volunteer/registerVolunteer")
	public String registerVolunteer(@RequestParam("volunteer_id") String volunteer_id,
			@RequestParam("email") String email, @RequestParam("age") String age, @RequestParam("name") String name,
			@RequestParam("contact") String contact, @RequestParam("city") String city,
			@RequestParam("language") String language,
			@RequestParam(value = "work_area_id[]") ArrayList<String> work_area_id,
			@RequestParam("platform") String platform, @RequestParam("time_slot") String time_slot,
			@RequestParam("orientation") String orientation, Model model) {
		log.info(work_area_id + " " + (platform));
		HashMap<String, Object> params = new HashMap<>();
		params.put("volunteer_id", volunteer_id);
		params.put("email", email);
		params.put("age", age);
		params.put("name", name);
		params.put("contact", contact);
		params.put("city", city);
		params.put("language", language);
		params.put("platform", platform);
		params.put("time_slot", time_slot);
		params.put("orientation", orientation);
		int result = workRepository.addVolunteer(params);
		log.info("Added " + result + " rows.");

		HashMap<String, Object> volunteer_work_area_map = new HashMap<>();
		volunteer_work_area_map.put("volunteer_id", volunteer_id);
		int workAreaResult = workRepository.addVolunteerWorkAreaMapping(volunteer_work_area_map);

		log.info("Work Area Result size: " + workAreaResult);
		return "welcome";
	}
}
