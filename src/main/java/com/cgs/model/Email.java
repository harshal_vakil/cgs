package com.cgs.model;

public class Email {
	private String emailTo;
	private String emailSubject;
	private String emailBody;

	public String getEmailBody() {
		return emailBody;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
}
