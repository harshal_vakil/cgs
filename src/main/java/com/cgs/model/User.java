package com.cgs.model;

import java.util.ArrayList;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class User {

	@NotEmpty(message = "Field cannot be empty")
	private String password;

	private String confirmPassword;
	@NotEmpty(message = "Field cannot be empty")
	@Pattern(regexp = "^(.+)@(.+)$")
	private String emailId;
	private String contactNo;
	private String firstName;
	private String lastName;

	private String roleId;
	private String age;
	private String state;
	private int cityId;
	private ArrayList<Integer> languageId;
	private String createdBy;
	private String createdDt;
	private String timeContributionFrequency;
	private int timeContributionHours;
	private String orientation;
	private String workAreaOthers;

	private String lastUpdateUI;
	private String lastUpdateTS;
	private String isApproved;
	private String approvedBy;

	public String getAge() {
		return age;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public int getCityId() {
		return cityId;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public String getContactNo() {
		return contactNo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getIsApproved() {
		return isApproved;
	}

	public ArrayList<Integer> getLanguageId() {
		return languageId;
	}

	public String getLastName() {
		return lastName;
	}

	public String getLastUpdateTS() {
		return lastUpdateTS;
	}

	public String getLastUpdateUI() {
		return lastUpdateUI;
	}

	public String getOrientation() {
		return orientation;
	}

	public String getPassword() {
		return password;
	}

	public String getRoleId() {
		return roleId;
	}

	public String getState() {
		return state;
	}

	public String getTimeContributionFrequency() {
		return timeContributionFrequency;
	}

	public int getTimeContributionHours() {
		return timeContributionHours;
	}

	public String getWorkAreaOthers() {
		return workAreaOthers;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}

	public void setLanguageId(ArrayList<Integer> languageId) {
		this.languageId = languageId;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setLastUpdateTS(String lastUpdateTS) {
		this.lastUpdateTS = lastUpdateTS;
	}

	public void setLastUpdateUI(String lastUpdateUI) {
		this.lastUpdateUI = lastUpdateUI;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setTimeContributionFrequency(String timeContributionFrequency) {
		this.timeContributionFrequency = timeContributionFrequency;
	}

	public void setTimeContributionHours(int timeContributionHours) {
		this.timeContributionHours = timeContributionHours;
	}

	public void setWorkAreaOthers(String workAreaOthers) {
		this.workAreaOthers = workAreaOthers;
	}
}
