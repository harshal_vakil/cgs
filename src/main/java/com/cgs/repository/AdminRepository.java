package com.cgs.repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AdminRepository extends BaseRepository {

	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");

	public boolean addNewDisease(Map<String, String[]> attributes) throws Exception {
		log.info("Adding new disease = " + attributes);
		String diseaseName = (attributes.get("diseaseName"))[0].toLowerCase();
		String email_id = (attributes.get("email_id"))[0];
		String remark = null;
		if (null != (attributes.get("remark")) && (attributes.get("remark")).length > 0)
			remark = (attributes.get("remark"))[0];
		Map<String, Object> params = new HashMap<>();
		params.put("disease_name", diseaseName);
		params.put("remark", remark);
		params.put("email_id", email_id);
		String checkIfDiseaseAlreadyExistsSql = "Select LOWER(disease_name) from diseases where disease_name = :disease_name";
		List<Map<String, Object>> diseaseExists = query(checkIfDiseaseAlreadyExistsSql, params);
		log.info("Disease exist: " + diseaseExists);
		if (!diseaseExists.isEmpty()) {
			return false;
		}
		String sql = "INSERT INTO diseases(disease_name, create_ui, last_update_ui) values(:disease_name, :email_id, :email_id)";
		int count = update(sql, params);
		if (count > 0) {
			log.info("Inserted disease successfully");
		}
		return true;
	}

	public void addNewService(Map<String, String[]> attributes) throws Exception {
		log.info("Adding new Service = " + attributes);
		String serviceName = (attributes.get("serviceName"))[0];
		String email_id = (attributes.get("email_id"))[0];
		String remark = null;
		if (null != (attributes.get("remark")) && (attributes.get("remark")).length > 0)
			remark = (attributes.get("remark"))[0];
		Map<String, Object> params = new HashMap<>();
		params.put("service_name", serviceName);
		params.put("remark", remark);
		params.put("email_id", email_id);
		String sql = "INSERT INTO services(service_name, remark, create_ui, last_update_ui) values(:service_name, :remark, :email_id, :email_id)";
		int count = update(sql, params);
		if (count > 0) {
			log.info("Service [" + serviceName + "] Inserted Successfully");
			sql = "select max(service_id) as service_id from services";
			List<Map<String, Object>> result = query(sql, null);
			int serviceId = Integer.parseInt(String.valueOf(result.get(0).get("service_id")));
			log.info("serviceId = " + serviceId);
			params.put("service_id", serviceId);
			String[] serviceAttributes = attributes.get("attribute");
			for (String attribute : serviceAttributes) {
				params.put("service_attribute_name", attribute);
				sql = "INSERT INTO service_attributes(service_id, service_attribute_name) values(:service_id, :service_attribute_name)";
				count = update(sql, params);
				if (count > 0)
					log.info("Attribute [" + attribute + "] Inserted Successfully");
			}
		}
	}

	public void approveRejectLead(Map<String, Object> params) {
		if (params.get("button").equals("approve")) {
			// perform approval logic
			// 1) Update vendor.
			String sql = "UPDATE VENDOR set vendor_id = :vendor_id, vendor_name = :vendor_name, city_id = :city_id, address = :vendor_address, pincode = :vendor_pincode, contact_no = :vendor_contact, last_update_ts = :last_update_ts WHERE vendor_id = :vendor_id";
			params.put("last_update_ts", getTimeStamp());

			int result = update(sql, params);
			log.info("Updated " + result + " vendor");

			// 2) insert into verified_leads
			String sql1 = "Select * from verified_leads where disease_id = :disease_id AND service_id = :service_id AND vendor_id = :vendor_id AND service_attribute_id = :service_sub_attribute_id";
			List<Map<String, Object>> verifiedLeadExisits = query(sql1, params);
			if (verifiedLeadExisits.isEmpty()) {
				// new entry in verified leads. Insert in table.
				String sql2 = "INSERT INTO verified_leads(disease_id, service_id, service_attribute_id, vendor_id, stock, create_ui, create_ts, last_update_ui, last_update_ts, approved_by, volunteer_id, volunteer_remarks, admin_remarks, volunteer_leads_verification_ts)"
						+ " values(:disease_id, :service_id, :service_sub_attribute_id, :vendor_id, :stock, :admin_email, :create_ts, :admin_email, :last_update_ts, :admin_email, :volunteer_id, :volunteer_remarks, :admin_remarks, :volunteer_leads_verification_ts)";
				params.put("create_ts", getTimeStamp());
				params.put("last_update_ts", getTimeStamp());
				params.put("volunteer_leads_verification_ts", getTimeStamp());
				int result1 = update(sql2, params);
				log.info("Inserted " + result1 + " record in verified leads table");
			} else {
				// update exisiting entry in verified_leads table.
				String sql2 = "UPDATE verified_leads set disease_id = :disease_id, service_id = :service_id, service_attribute_id = :service_sub_attribute_id, vendor_id = :vendor_id, stock = :stock, last_update_ui = :admin_email, last_update_ts = :last_update_ts, approved_by = :admin_email, volunteer_id = :volunteer_id, volunteer_remarks = :volunteer_remarks, admin_remarks = :admin_remarks, volunteer_leads_verification_ts = :volunteer_leads_verification_ts"
						+ " WHERE disease_id = :disease_id AND service_id = :service_id AND vendor_id = :vendor_id AND service_attribute_id = :service_sub_attribute_id";
				params.put("last_update_ts", getTimeStamp());
				params.put("volunteer_leads_verification_ts", getTimeStamp());
				int result1 = update(sql2, params);
				log.info("Updated " + result1 + " record in verified leads table");
			}
			int performanceId = getMaxPerformanceId(params);

			// 3) update performance
			String sql3 = "UPDATE performance set stock = :stock, last_update_ui = :admin_email, last_update_ts = :last_update_ts, admin_remarks = :admin_remarks, admin_verification_ts = :admin_verification_ts, is_approved = :is_approved"
					+ " WHERE performance_id = :performance_id";
			params.put("last_update_ts", getTimeStamp());
			params.put("admin_verification_ts", getTimeStamp());
			params.put("is_approved", "Y");
			params.put("performance_id", performanceId);
			int result2 = update(sql3, params);
			log.info("Updated " + result2 + " record in performance table");

			markStaleEntryAsNotApplicable(params, "approve");

			// 4) delete from unverified_leads
			String sql4 = "DELETE FROM unverified_leads where unverified_leads_id = :unverified_leads_id";
			int result3 = update(sql4, params);
			log.info("Deleted " + result3 + " row from unverified_leads table");

		} else {
			// perform rejection logic
			int performanceId = getMaxPerformanceId(params);
			// 1)Update performance
			String sql = "UPDATE performance set stock = :stock, last_update_ui = :admin_email, last_update_ts = :last_update_ts, admin_remarks = :admin_remarks, admin_verification_ts = :admin_verification_ts, is_approved = :is_approved"
					+ " WHERE performance_id = :performance_id";
			params.put("last_update_ts", getTimeStamp());
			params.put("admin_verification_ts", getTimeStamp());
			params.put("is_approved", "N");
			params.put("performance_id", performanceId);
			int result = update(sql, params);
			log.info("Updated " + result + " record in performance table");

			markStaleEntryAsNotApplicable(params, "reject");

			// 2) Delete from unverified_leads
			String sql1 = "DELETE FROM unverified_leads where unverified_leads_id = :unverified_leads_id";
			result = update(sql1, params);
			log.info("Deleted " + result + " row from unverified_leads table");
		}
	}

	public void approveUser(Map<String, Object> params) {
		log.info("Approving user." + params);
		String sql = "Update users set is_approved = :is_approved, approved_by = :hidden_email_id where email_id = :email_id";
		int result = update(sql, params);
		log.info("Updated " + result + " in users table");

	}

	public Map<String, Object> getApprovalDetail(Map<String, Object> params) {
		String sql = "SELECT * from unverified_leads where unverified_leads_id = :unverified_leads_id";
		List<Map<String, Object>> result = query(sql, params);
		Map<String, Object> details = new HashMap<>();
		details.put("unverified_leads_id", result.get(0).get("unverified_leads_id"));
		if (!result.isEmpty()) {
			log.info("Unverified Lead ===>" + result.toString());
			Map<String, Object> params1 = new HashMap<>();
			params1.put("vendor_id", result.get(0).get("vendor_id"));
			details.put("volunteer_id", result.get(0).get("volunteer_id"));
			details.put("volunteer_remarks", result.get(0).get("volunteer_remarks"));
			details.put("vendor_id", result.get(0).get("vendor_id"));
			details.put("service_id", result.get(0).get("service_id"));
			details.put("service_attribute_id", result.get(0).get("service_attribute_id"));
			details.put("disease_id", result.get(0).get("disease_id"));

			String sql1 = "Select * from vendor where vendor_id = :vendor_id";
			List<Map<String, Object>> result1 = query(sql1, params1);
			if (!result1.isEmpty()) {
				log.info("Unverified Lead Vendor ===>" + result1.toString());

				Map<String, Object> params2 = new HashMap<>();
				params2.put("city_id", result1.get(0).get("city_id"));
				String sql2 = "Select * from cities where city_id = :city_id";
				List<Map<String, Object>> result2 = query(sql2, params2);
				if (!result2.isEmpty()) {
					details.put("city_id", result2.get(0).get("city_id"));
					details.put("city_name", result2.get(0).get("city_name"));
					details.put("city_state", result2.get(0).get("city_state"));
				}
				details.put("vendor_name", result1.get(0).get("vendor_name"));
				details.put("address", result1.get(0).get("address"));
				details.put("pincode", result1.get(0).get("pincode"));
				details.put("contact_no", result1.get(0).get("contact_no"));

				Map<String, Object> params3 = new HashMap<>();
				params3.put("service_attribute_id", result.get(0).get("service_attribute_id"));
				String sql3 = "Select * from service_attributes where service_attribute_id = :service_attribute_id";
				List<Map<String, Object>> result3 = query(sql3, params3);
				if (!result3.isEmpty()) {

					Map<String, Object> params4 = new HashMap<>();
					params4.put("service_id", result3.get(0).get("service_id"));
					String sql4 = "Select * from services where service_id = :service_id";
					List<Map<String, Object>> result4 = query(sql4, params4);
					if (!result4.isEmpty()) {
						details.put("service_name", result4.get(0).get("service_name"));
					}

					details.put("service_attribute_name", result3.get(0).get("service_attribute_name"));
					details.put("stock", result.get(0).get("stock"));
				}

				Map<String, Object> params5 = new HashMap<>();
				params5.put("disease_id", result.get(0).get("disease_id"));
				String sql5 = "Select * from diseases where disease_id = :disease_id";
				List<Map<String, Object>> result5 = query(sql5, params5);
				if (!result5.isEmpty()) {
					details.put("disease_name", result5.get(0).get("disease_name"));
				}
			}
		}
		log.info("Unverified Lead Details ===>" + details.toString());
		return details;
	}

	public List<Map<String, Object>> getApprovals() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT ul.unverified_leads_id,v.vendor_name, volunteer_leads_verification_ts, stock, concat(u.first_name, ' ', u.last_name) AS volunteer_name, d.disease_name, s.service_name, sa.service_attribute_name FROM unverified_leads ul INNER JOIN vendor v ON ul.vendor_id=v.vendor_id INNER JOIN diseases d ON d.disease_id = ul.disease_id INNER JOIN services s ON s.service_id = ul.service_id INNER JOIN service_attributes sa ON sa.service_attribute_id = ul.service_attribute_id INNER JOIN users u ON u.email_id = ul.volunteer_id";
		List<Map<String, Object>> result = query(sql, params);
		/*
		 * String sql1 =
		 * "SELECT first_name, last_name from users where email_id = :volunteer_id";
		 * List<Map<String, Object>> result1 = new ArrayList<>(); for (int i = 0; i <
		 * result.size(); i++) { params.put("volunteer_id",
		 * result.get(i).get("volunteer_id")); result1 = query(sql1, params);
		 * result.get(i).put("volunteer_first_name", result1.get(0).get("first_name"));
		 * result.get(i).put("volunteer_last_name", result1.get(0).get("last_name")); }
		 * result1.clear(); String sql2 =
		 * "Select disease_name from diseases where disease_id = :disease_id"; for (int
		 * i = 0; i < result.size(); i++) { params.put("disease_id",
		 * result.get(i).get("disease_id")); result1 = query(sql2, params);
		 * result.get(i).put("disease_name", result1.get(0).get("disease_name")); }
		 * result1.clear(); String sql3 =
		 * "Select service_attribute_name from service_attributes where service_attribute_id = :service_attribute_id AND service_id = :service_id "
		 * ; for (int i = 0; i < result.size(); i++) { if
		 * (result.get(i).get("service_attribute_id") == null) {
		 * result.get(i).put("service_attribute_name", null); } else {
		 * params.put("service_attribute_id",
		 * result.get(i).get("service_attribute_id")); params.put("service_id",
		 * result.get(i).get("service_id")); result1 = query(sql3, params);
		 * result.get(i).put("service_attribute_name",
		 * result1.get(0).get("service_attribute_name")); } } String sql4 =
		 * "Select service_name from services where service_id = :service_id ";
		 */
		for (int i = 0; i < result.size(); i++) {
			// params.put("service_id", result.get(i).get("service_id"));
			// result1 = query(sql4, params);
			// result.get(i).put("service_name", result1.get(0).get("service_name"));
			Date date = (Date) result.get(i).get("volunteer_leads_verification_ts");
			result.get(i).put("volunteer_leads_verification_ts",
					"<span class='hidden-time'>" + date.getTime() + "-</span>" + sdf.format(date));
		}
		return result;
	}

	public int getMaxPerformanceId(Map<String, Object> params) {
		String sql6 = "Select performance_id from performance where unverified_leads_id = :unverified_leads_id";
		List<Map<String, Object>> performanceIdMap = query(sql6, params);
		int performanceId = -1;
		for (int i = 0; i < performanceIdMap.size(); i++) {
			if (performanceId < (int) performanceIdMap.get(i).get("performance_id")) {
				performanceId = (int) performanceIdMap.get(i).get("performance_id");
			}
		}
		return performanceId;
	}

	public Date getTimeStamp() {
		return new Date();
	}

	public void markStaleEntryAsNotApplicable(Map<String, Object> params, String approveOrReject) {
		String sql5 = "UPDATE performance set is_approved = :is_approved_na"
				+ " WHERE unverified_leads_id = :unverified_leads_id AND is_approved != :is_approved";
		params.put("last_update_ts", getTimeStamp());
		params.put("admin_verification_ts", getTimeStamp());
		if (approveOrReject.equals("approve")) {
			params.put("is_approved", "Y");
		} else {
			params.put("is_approved", "N");
		}
		params.put("is_approved_na", "NA");
		int result4 = update(sql5, params);
		log.info("Updated " + result4 + " record in performance table");
	}

	public int toggleDisease(Map<String, Object> params) {
		log.info("Enable/Disable Disease = " + params);
		String sql = "UPDATE diseases set enabled = :toggle_string where disease_id = :disease_id";
		int result = update(sql, params);
		return result;
	}

	public int toggleService(Map<String, Object> params) {
		log.info("Enable/Disable Service = " + params);
		String sql = "UPDATE services set enabled = :toggle_string where service_id = :service_id";
		int result = update(sql, params);
		return result;
	}
}
