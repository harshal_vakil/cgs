package com.cgs.repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.cgs.util.CGSUtil;
import com.cgs.util.PasswordUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseRepository {

	@Autowired
	PasswordUtil passwordUtil;

	@Autowired
	CGSUtil cgsUtil;

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	public List<Map<String, Object>> query(String sql, Map<String, Object> params) {
		List<Map<String, Object>> result = new LinkedList<>();
		try {
			result = jdbcTemplate.queryForList(sql.toLowerCase(), params);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return result;
	}

	public int update(String sql, Map<String, Object> params) {
		int result = 0;
		result = jdbcTemplate.update(sql.toLowerCase(), params);
		return result;
	}
}
