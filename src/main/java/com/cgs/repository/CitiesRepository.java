package com.cgs.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

// @Slf4j
@Component
public class CitiesRepository extends BaseRepository {

	public List<Map<String, Object>> getAllCitiesbyStates(Map<String, Object> params) {
		String sql = "SELECT distinct city_id, city_name FROM cities where city_state=:state order by city_name";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<String> getAllStates() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT distinct city_state FROM cities order by city_state";
		List<Map<String, Object>> result = query(sql, params);
		List<String> states = new ArrayList<>();
		for (Map<String, Object> map : result) {
			String state = String.valueOf(map.get("city_state"));
			states.add(state);
		}
		return states;
	}

	public Map<String, Object> getCityById(Map<String, Object> params) {
		String sql = "SELECT * FROM cities where city_id=:city_id";
		List<Map<String, Object>> result = query(sql, params);
		Map<String, Object> city = new HashMap<>();
		if (!result.isEmpty())
			city = result.get(0);
		return city;
	}

}
