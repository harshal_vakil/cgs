package com.cgs.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

// @Slf4j
@Component
public class DiseaseRepository extends BaseRepository {

	public List<Map<String, Object>> getAllDiseases() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM DISEASES where sort_id <> 0 ORDER BY sort_id, disease_name";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getAllEnabledDiseases() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM DISEASES WHERE ENABLED='Y' ORDER BY sort_id, disease_name";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getDiseaseFromService(Map<String, Object> params) {
		String sql = "SELECT d.* FROM DISEASES d inner join disease_service_mapping dsm on dsm.SERVICE_ID= d.SERVICE_ID on WHERE dsm.SERVICE_ID=:service_id";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getServiceFromDisease(Map<String, Object> params) {
		String sql = "SELECT s.* FROM service s inner join disease_service_mapping dsm on dsm.SERVICE_ID= s.SERVICE_ID on WHERE dsm.SERVICE_ID=:service_id";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}
}
