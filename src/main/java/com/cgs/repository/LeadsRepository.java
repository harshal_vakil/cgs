package com.cgs.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SuppressWarnings("unchecked")
public class LeadsRepository extends BaseRepository {

	public String createLead(HashMap<String, Object> params) {
		log.info("Create lead params: " + params);
		String vendorId = String.valueOf(params.get("vendor_id"));
		// check if params has vendorId set
		if (StringUtils.isBlank(vendorId) || vendorId.equalsIgnoreCase("null")) {
			// Insert first in vendor table
			params.put("create_ts", getTimeStamp());
			params.put("last_update_ts", getTimeStamp());
//			params.put("approved_by", null);
			String sql = "INSERT INTO VENDOR(vendor_name, city_id, address, pincode, contact_no, create_ui, create_ts, last_update_ui, last_update_ts) VALUES(:vendor_name, :city, :vendor_address, :pincode, :contact_number, :volunteer_id, :create_ts, :volunteer_id, :last_update_ts)";
			int count = update(sql, params);
			log.info("Created: " + count + " new vendor in vendor table");
			if (count > 0) {
				// get vendorId from vendor table
				sql = "SELECT max(vendor_id) as vendor_id from vendor";
				List<Map<String, Object>> result = query(sql, null);
				int vendor_id = Integer.parseInt(String.valueOf(result.get(0).get("vendor_id")));
				vendorId = String.valueOf(vendor_id);
				params.put("vendor_id", vendor_id);
				log.info("New created vendorId: " + vendor_id);

				// insert data in unverified leads table;
				insertInUnverifiedLeads(params);
			}
			return vendorId;
		} else {
			insertInUnverifiedLeads(params);
		}
		return vendorId;
	}

	public int createVendor(Map<String, Object> params) {
		log.info("Add volunteer params: " + params);
		String sql = "INSERT INTO vendor (vendor_name, email_id, contact_no, city, state, pincode)"
				+ " VALUES(:vendor_name, :email_id, :contact_no, :city, :state, :pincode)";
		int result = update(sql, params);
		return result;
	}

	public List<Map<String, Object>> getExisitingVendorsDetails(Map<String, Object> params) {
		String sql = "SELECT * from vendor where vendor_id = :vendor_id";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getExistingVendors(Map<String, Object> params) {
		String sql = "SELECT vendor_id, CONCAT(vendor_name, ' - ', pincode) AS vendor_name FROM vendor WHERE city_id=:city_id ORDER BY 2";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public Date getTimeStamp() {
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//		LocalDateTime now = LocalDateTime.now();
//		return dtf.format(now);
		return new Date();
	}

	public Map<String, Object> getVendorById(String vendorId) {
		Map<String, Object> params = new HashMap<>();
		params.put("vendor_id", vendorId);
		log.info("getVendorById params: " + vendorId);
		String sql = "SELECT * FROM vendor v INNER JOIN cities c ON c.city_id=v.city_id WHERE v.vendor_id=:vendor_id";
		List<Map<String, Object>> result = query(sql, params);
		Map<String, Object> vendor = new HashMap<>();
		if (!result.isEmpty())
			vendor = result.get(0);

		return vendor;
	}

	public void insertIntoPerformance(HashMap<String, Object> params) {
		String sql = "Select unverified_leads_id from unverified_leads where disease_id = :disease AND vendor_id = :vendor_id AND volunteer_id = :volunteer_id AND service_id = :service_type AND service_attribute_id = :service_attribute_id";
		List<Map<String, Object>> unverifiedLeadsIdMap = query(sql, params);
		for (int i = 0; i < unverifiedLeadsIdMap.size(); i++) {
			params.put("unverified_leads_id", unverifiedLeadsIdMap.get(i).get("unverified_leads_id"));
		}

		sql = "INSERT INTO performance(unverified_leads_id, service_id, disease_id, service_attribute_id, vendor_id, stock, create_ui, create_ts, last_update_ui, last_update_ts, volunteer_id, volunteer_remarks, volunteer_leads_verification_ts) "
				+ "values(:unverified_leads_id, :service_type, :disease, :service_attribute_id, :vendor_id, :stock, :volunteer_id, :create_ts, :volunteer_id, :last_update_ts, :volunteer_id, :remarks, :volunteer_leads_verification_ts)";
		int performanceCount = update(sql, params);
		log.info("Inserted: " + performanceCount + " row(s) in performance");
	}

	public void insertInUnverifiedLeads(HashMap<String, Object> params) {
//		String sql = "";
		// check if update for same volunteer or insert for a new volunteer for same
		// service + disease + vendor id

		if (params.get("service_attribute_id") == null) {
			// insert null in unverified_leads table for serviceAttributeId and stock
			params.put("create_ts", getTimeStamp());
			params.put("last_update_ts", getTimeStamp());
			params.put("volunteer_leads_verification_ts", getTimeStamp());
//			sql = "INSERT INTO unverified_leads(disease_id, service_attribute_id, vendor_id, stock, create_ui, create_ts, last_update_ui, last_update_ts, volunteer_id, city_id, address, pincode, contact_no, volunteer_remarks, volunteer_leads_verification_ts) values(:disease, :service_attribute_id, :vendor_id, :stock, :volunteer_id, :create_ts, :volunteer_id, :last_update_ts, :volunteer_id, :city, :vendor_address, :pincode, :contact_number, :remarks, :volunteer_leads_verification_ts)";
//			int unverifiedLeadsCount = update(sql, params);
//			log.info("Inserted " + unverifiedLeadsCount + " row(s) in table.");
			updateOrInsertUnverifiedLeads(params);
		} else {
			// iterate over serviceAttributeId and stocks to insert that many rows in table
			params.put("create_ts", getTimeStamp());
			params.put("last_update_ts", getTimeStamp());
			params.put("volunteer_leads_verification_ts", getTimeStamp());
			ArrayList<String> serviceAttributeId = (ArrayList<String>) params.get("service_attribute_id");
			ArrayList<String> stock = (ArrayList<String>) params.get("stock");
			for (int i = 0; i < serviceAttributeId.size(); i++) {
				String attributeId = serviceAttributeId.get(i);
				String stck = stock.get(i);
				log.info("Service Attribute Id: " + attributeId);
				log.info("Stock: " + stck);
				params.put("service_attribute_id", attributeId);
				params.put("stock", stck);
				if (StringUtils.isNotBlank(stck))
					updateOrInsertUnverifiedLeads(params);
//				sql = "INSERT INTO unverified_leads(disease_id, service_attribute_id, vendor_id, stock, create_ui, create_ts, last_update_ui, last_update_ts, volunteer_id, city_id, address, pincode, contact_no, volunteer_remarks, volunteer_leads_verification_ts) values(:disease, :service_attribute_id, :vendor_id, :stock, :volunteer_id, :create_ts, :volunteer_id, :last_update_ts, :volunteer_id, :city, :vendor_address, :pincode, :contact_number, :remarks, :volunteer_leads_verification_ts)";
//				int unverifiedLeadsCount = update(sql, params);
//				log.info("Inserted " + unverifiedLeadsCount + " row(s) in table.");
			}
		}
	}

	public void updateOrInsertUnverifiedLeads(HashMap<String, Object> params) {
		String sql = "";
		// check if update for same volunteer or insert for a new volunteer for same
		// service + disease + vendor id
		String serviceAttributeId = (String) params.get("service_attribute_id");
		if (serviceAttributeId == null) {
			sql = "Select * from unverified_leads where disease_id = :disease AND vendor_id = :vendor_id AND volunteer_id = :volunteer_id AND service_id = :service_type";
		} else {
			sql = "Select * from unverified_leads where disease_id = :disease AND vendor_id = :vendor_id AND volunteer_id = :volunteer_id AND service_id = :service_type AND service_attribute_id = :service_attribute_id";
		}

		List<Map<String, Object>> updateOrInsertMap = query(sql, params);
		log.info("updateOrInsertMap: " + updateOrInsertMap);
		if (updateOrInsertMap.size() == 0) {
			// insert.
			params.put("create_ts", getTimeStamp());
			params.put("last_update_ts", getTimeStamp());
			params.put("volunteer_leads_verification_ts", getTimeStamp());
			sql = "INSERT INTO unverified_leads(disease_id, service_id, service_attribute_id, vendor_id, stock, create_ui, create_ts, last_update_ui, last_update_ts, volunteer_id, city_id, address, pincode, contact_no, volunteer_remarks, volunteer_leads_verification_ts) "
					+ "values(:disease, :service_type, :service_attribute_id, :vendor_id, :stock, :volunteer_id, :create_ts, :volunteer_id, :last_update_ts, :volunteer_id, :city, :vendor_address, :pincode, :contact_number, :remarks, :volunteer_leads_verification_ts)";
			int unverifiedLeadsCount = update(sql, params);
			log.info("Inserted " + unverifiedLeadsCount + " row(s) in table.");
		} else {
			// update
//			params.put("create_ts", getTimeStamp());
			params.put("last_update_ts", getTimeStamp());
			params.put("volunteer_leads_verification_ts", getTimeStamp());
			log.info("VolunteerId in updateOrInsertFunction params: " + params.get("volunteer_id"));
			sql = "UPDATE unverified_leads SET disease_id = :disease, service_id = :service_type, service_attribute_id = :service_attribute_id, vendor_id = :vendor_id, stock = :stock, create_ui = :volunteer_id, last_update_ui = :volunteer_id, last_update_ts = :last_update_ts, volunteer_id = :volunteer_id, city_id = :city, address = :vendor_address, pincode = :pincode, contact_no = :contact_number, volunteer_remarks = :remarks, volunteer_leads_verification_ts = :volunteer_leads_verification_ts WHERE disease_id = :disease AND service_attribute_id = :service_attribute_id AND vendor_id = :vendor_id AND volunteer_id = :volunteer_id";
			int unverifiedLeadsCount = update(sql, params);
			log.info("Updated " + unverifiedLeadsCount + " row(s) in table.");
		}
		insertIntoPerformance(params);
	}
}
