package com.cgs.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SuppressWarnings({ "unchecked", "serial" })
public class PerformanceRepository extends UserRepository {

	public List<Map<String, Object>> getAllLeads() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM PERFORMANCE where is_approved != 'NA'";
		return query(sql, params);
	}

	private int getPercentage(int correct, int total) {
		return correct * 100 / total;
	}

	public List<Map<String, Object>> getVolunteerPerformance() {
		Map<String, Object> volunteerPerformanceMap = new HashMap<>();
		List<Map<String, Object>> allLeads = getAllLeads();
		for (Map<String, Object> lead : allLeads) {
			if (!lead.get("is_approved").toString().equals("P")) {
				log.info(String.valueOf(lead));
				Map<String, Object> user = findUserbyEmail(new HashMap<String, Object>() {
					{
						put("email_id", lead.getOrDefault("volunteer_id", "harshal.d.vakil@tsgforce.com"));
					}
				}).get(0);

				String volunteerName = user.get("first_name") + " " + user.get("last_name");

				if (!volunteerPerformanceMap.containsKey(volunteerName)) {
					volunteerPerformanceMap.put(volunteerName, new HashMap<String, Integer>());
				}

				HashMap<String, Integer> performanceDetails = (HashMap<String, Integer>) volunteerPerformanceMap
						.get(volunteerName);

				if (lead.get("is_approved").toString().equals("Y")) {
					performanceDetails.put("correct_leads", performanceDetails.getOrDefault("correct_leads", 0) + 1);
				} else if (lead.get("is_approved").toString().equals("N")) {
					performanceDetails.put("incorrect_leads",
							performanceDetails.getOrDefault("incorrect_leads", 0) + 1);
				}

				performanceDetails.put("total_leads", performanceDetails.getOrDefault("total_leads", 0) + 1);

				volunteerPerformanceMap.replace(volunteerName, performanceDetails);
			}
		}

		List<Map<String, Object>> volunteerPerformance = new ArrayList<>();
		for (String volunteerName : volunteerPerformanceMap.keySet()) {
			HashMap<String, Object> performanceDetails = (HashMap<String, Object>) volunteerPerformanceMap
					.get(volunteerName);

			int percentage = getPercentage((Integer) performanceDetails.getOrDefault("correct_leads", 0),
					(Integer) performanceDetails.getOrDefault("total_leads", 0));

			if (!performanceDetails.containsKey("correct_leads")) {
				performanceDetails.put("correct_leads", 0);
			}
			if (!performanceDetails.containsKey("incorrect_leads")) {
				performanceDetails.put("incorrect_leads", 0);
			}

			performanceDetails.put("performance_accuracy", percentage);

			performanceDetails.put("volunteer_name", volunteerName);

			volunteerPerformance.add(performanceDetails);
		}

		return volunteerPerformance;
	}

}
