package com.cgs.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ServicesAttributeRepository extends BaseRepository {

	public List<Map<String, Object>> getEditServiceAttributes(Map<String, Object> params) {
		log.info("Getting service sub attributes for vendor : " + params.get("vendor_id"));
		String sql = "SELECT * from service_attributes WHERE service_id = :service_id";
		List<Map<String, Object>> result = query(sql, params);
		log.info("Service Attributes: " + result);
		List<Map<String, Object>> finalResult = new ArrayList<>();
//		for(int i = 0; i < result.size(); i++) {

//			params.put("service_attribute_id", result.get(i).get("service_attribute_id"));
		sql = "SELECT vl.disease_id, sa.service_attribute_id, vl.vendor_id, IFNULL(vl.stock, 0) AS stock \n"
				+ "FROM service_attributes sa left outer join verified_leads vl on vl.service_attribute_id = sa.service_attribute_id AND vl.disease_id = IFNULL(:disease_id, vl.disease_id) AND vl.vendor_id = IFNULL(:vendor_id, vl.vendor_id)\n"
				+ "WHERE sa.service_id = :service_id";
		finalResult = query(sql, params);
		log.info(String.valueOf(result));
//			if(tempResult.size() > 0) {
//				finalResult.add(tempResult.get(0));
//			}
//		}
		log.info(String.valueOf(finalResult));
		return finalResult;
	}

	public List<Map<String, Object>> getServiceAttributes(Map<String, Object> params) {
		log.info("Getting service sub attributes");
		String sql = "Select * from service_attributes where service_id = :service_id";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}
}
