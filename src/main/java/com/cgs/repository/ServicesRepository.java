package com.cgs.repository;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

// @Slf4j
@Component
public class ServicesRepository extends BaseRepository {

	public List<Map<String, Object>> getAllEnabledServices() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM SERVICES where enabled = 'Y'";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getAllServices() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM SERVICES";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getFilteredServiceVendors(Map<String, Object> params) {
		String sql = "SELECT DISTINCT v.vendor_id, v.vendor_name FROM verified_leads vl INNER JOIN service_attributes sa ON sa.service_attribute_id = vl.service_attribute_id INNER JOIN vendor v ON v.vendor_id = vl.vendor_id INNER JOIN cities c ON c.city_id = v.city_id INNER JOIN diseases d ON d.disease_id = vl.disease_id WHERE sa.SERVICE_ID=:service_id AND v.pincode = IFNULL(:pincode, v.pincode) AND c.city_id = IFNULL(:city_id, c.city_id) AND d.disease_id = IFNULL(:disease_id, d.disease_id) ORDER BY v.vendor_name";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public Map<String, Object> getServiceDetails(Map<String, Object> params) {
		String sql = "SELECT * FROM services s INNER JOIN service_attributes sa ON sa.service_id = s.service_id WHERE sa.SERVICE_ID=:service_id;";
		List<Map<String, Object>> result = query(sql, params);
		Map<String, Object> details = new HashMap<>();
		if (!result.isEmpty())
			details = result.get(0);
		LinkedHashSet<String> attributes = new LinkedHashSet<>();
		for (Map<String, Object> map : result) {
			attributes.add(String.valueOf(map.get("service_attribute_name")));
		}
		details.put("attributes", attributes);
		return details;
	}

	public Map<String, Object> getServiceVendorDetails(Map<String, Object> params) {
		String sql = "SELECT DISTINCT * FROM verified_leads vl INNER JOIN service_attributes sa ON sa.service_attribute_id = vl.service_attribute_id INNER JOIN vendor v ON v.vendor_id = vl.vendor_id INNER JOIN cities c ON c.city_id = v.city_id INNER JOIN diseases d ON d.disease_id = vl.disease_id WHERE sa.SERVICE_ID=:service_id AND v.vendor_id=:vendor_id AND (d.disease_id= :disease_id OR (:disease_id ='' AND d.disease_id=d.disease_id)) ORDER BY sa.service_attribute_name";
		List<Map<String, Object>> result = query(sql, params);
		Map<String, Object> details = result.get(0);
		Map<String, String> attributes = new LinkedHashMap<>();
		for (Map<String, Object> map : result) {
			attributes.put(String.valueOf(map.get("service_attribute_name")), String.valueOf(map.get("stock")));
		}
		details.put("attributes", attributes);
		return details;
	}

	public List<Map<String, Object>> getServiceVendors(Map<String, Object> params) {
		String sql = "SELECT DISTINCT v.vendor_id, v.vendor_name, vl.disease_id FROM verified_leads vl INNER JOIN service_attributes sa ON sa.service_attribute_id = vl.service_attribute_id INNER JOIN vendor v ON v.vendor_id = vl.vendor_id WHERE sa.SERVICE_ID=:service_id and vl.disease_id=:disease_id ORDER BY V.VENDOR_NAME";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}
}
