package com.cgs.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

// @Slf4j
@Component
public class UserRepository extends BaseRepository {

	public List<Map<String, Object>> findUserbyEmail(Map<String, Object> params) {
		String sql = "Select * from users u inner join roles r on r.role_id = u.role_id where u.email_id=:email_id";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> findUserbyEmailPassword(Map<String, Object> params) {
		params.put("is_approved", "Y");
		String sql = "Select * from users u inner join roles r on r.role_id = u.role_id where u.email_id=:email_id and u.password=:password and is_approved = :is_approved";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getAllLanguages() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT language_id,language_name FROM LANGUAGES";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getAllRoles() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM roles";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getAllUnapprovedUsers() {
		Map<String, Object> params = new HashMap<>();
		params.put("is_approved", "N");
		String sql = "SELECT * FROM users where is_approved = :is_approved";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public List<Map<String, Object>> getAllUsers() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM users";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}

	public int register(Map<String, Object> params) {
		String sql = "INSERT INTO users (email_id, password, first_name, last_name, contact_no, role_id, age, create_ui, create_ts, city_id, language_id, time_contribution_frequency, time_contribution_hours, orientation) "
				+ "VALUES (:email_id, :password, :first_name, :last_name, :contact_no, :role_id, :age, :create_ui, :create_ts, :city_id, :language_id, :time_contribution_frequency, :time_contribution_hours, :orientation)";
		int result = update(sql, params);
		return result;
	}
}
