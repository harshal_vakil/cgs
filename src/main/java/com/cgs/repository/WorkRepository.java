package com.cgs.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WorkRepository extends BaseRepository {
	public int addVolunteer(HashMap<String, Object> params) {
		log.info("Add volunteer params: " + params);
		String sql = "INSERT INTO VOLUNTEER (volunteer_id, email, age, contact, full_name, city, languages, know_us, time_contribution, orientation)"
				+ "VALUES(:volunteer_id, :email, :age, :contact, :name, :city, :language, :platform, :time_slot, :orientation)";
		int result = update(sql, params);
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int addVolunteerWorkAreaMapping(HashMap<String, Object> volunteer_work_area_map) {
		log.info("Volunteer Work Area Map: " + volunteer_work_area_map);
		int result = 0;
		String volunteer_id = (String) volunteer_work_area_map.get("volunteer_id");

		ArrayList<String> work_area_array = (ArrayList) volunteer_work_area_map.get("work_area_id");
		String sql = "INSERT INTO VOLUNTEER_WORK_AREA_MAPPING (volunteer_id, work_area_id, create_ui, last_update_ui) VALUES(:volunteer_id, :work_area_id, :volunteer_id, :volunteer_id)";
		HashMap<String, Object> params = new HashMap<>();
		params.put("volunteer_id", volunteer_id);
		int work_area_id_length = work_area_array.size();
		for (int i = 0; i < work_area_id_length; i++) {
			String work_area_id = work_area_array.get(i);
			params.put("work_area_id", work_area_id);
			result = update(sql, params);
			log.info("Inserted " + result + " row.");
		}
		return result;
	}

	public List<Map<String, Object>> getAllWorkAreas() {
		Map<String, Object> params = new HashMap<>();
		String sql = "SELECT * FROM WORK_AREA";
		List<Map<String, Object>> result = query(sql, params);
		return result;
	}
}
