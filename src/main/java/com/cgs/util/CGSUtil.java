package com.cgs.util;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CGSUtil {

	public <T> T convertJsonToObject(String json, Class<T> clazz) {
		T object = null;
		try {
			object = new ObjectMapper().readValue(json, clazz);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return object;
	}

	public String convertObjectToJson(Object object) {
		String json = "";
		try {
			json = new ObjectMapper().writeValueAsString(object);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return json;
	}
}
