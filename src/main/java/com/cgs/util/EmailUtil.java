package com.cgs.util;
// File Name SendHTMLEmail.java

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cgs.model.Email;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EmailUtil {

	@Value("${spring.mail.username}")
	String from;

	@Value("${spring.mail.password}")
	String password;

	public void sendMail(Email email) {

		String to = email.getEmailTo();
		String subject = email.getEmailSubject();
		String msg = email.getEmailBody();

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});

		session.setDebug(true);

		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setContent(msg, "text/html; charset=utf-8");
			log.info("Email To = " + email.getEmailTo());
			log.info("Email Subject = " + email.getEmailSubject());
			log.debug("Email Sending...");
			Transport.send(message);
			log.debug("Email Sent message successfully....");
		} catch (MessagingException e) {
			log.error(e.getMessage(), e);
		}
	}

}