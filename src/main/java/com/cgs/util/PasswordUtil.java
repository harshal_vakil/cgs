package com.cgs.util;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PasswordUtil {

	private static final String UNICODE_FORMAT = "UTF8";

	public static void main(String[] args) throws Exception {
		PasswordUtil util = new PasswordUtil();
		log.info(util.encrypt("123456"));
		log.info(util.decrypt("CDAKcdLDNBY="));
	}

	private KeySpec ks;
	private SecretKeyFactory skf;
	private Cipher cipher;
	private String myEncryptionKey = "CareGiverSaathiEncryptionKey";
	private String myEncryptionScheme = "DESede";
	SecretKey key;

	byte[] arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);

	public PasswordUtil() throws Exception {
		ks = new DESedeKeySpec(arrayBytes);
		skf = SecretKeyFactory.getInstance(myEncryptionScheme);
		cipher = Cipher.getInstance(myEncryptionScheme);
		key = skf.generateSecret(ks);
	}

	public String decrypt(String encryptedString) {
		String decryptedText = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] encryptedText = Base64.decodeBase64(encryptedString);
			byte[] plainText = cipher.doFinal(encryptedText);
			decryptedText = new String(plainText);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return decryptedText;
	}

	public String encrypt(String unencryptedString) {
		String encryptedString = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] encryptedText = cipher.doFinal(plainText);
			encryptedString = new String(Base64.encodeBase64(encryptedText));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return encryptedString;
	}
}
