-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.28 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6550
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for caregiversaathi
DROP DATABASE IF EXISTS `caregiversaathi`;
CREATE DATABASE IF NOT EXISTS `caregiversaathi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `caregiversaathi`;

-- Dumping structure for table caregiversaathi.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int unsigned NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL DEFAULT '',
  `city_state` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_name_city_state` (`city_name`,`city_state`)
) ENGINE=InnoDB AUTO_INCREMENT=385 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.cities: ~58 rows (approximately)
DELETE FROM `cities`;
INSERT INTO `cities` (`city_id`, `city_name`, `city_state`) VALUES
	(37, 'Agra', 'Uttar Pradesh'),
	(6, 'Ahmedabad', 'Gujarat'),
	(42, 'Allahabad (Prayagraj)', 'Uttar Pradesh'),
	(56, 'Alwar', 'Rajasthan'),
	(17, 'Ballari ', 'Karnataka'),
	(18, 'Belagavi (Belgaum)', 'Karnataka'),
	(4, 'Bengaluru ', 'Karnataka'),
	(10, 'Bhopal', 'Madhya Pradesh'),
	(16, 'Bhubaneshwar', 'Odisha'),
	(12, 'Chandigarh', 'Punjab'),
	(3, 'Chennai', 'Tamil Nadu'),
	(47, 'Chittor', 'Rajasthan'),
	(19, 'Coimbatore', 'Tamil Nadu'),
	(20, 'Cuttack', 'Odisha'),
	(21, 'Dehradun', 'Uttarakhand'),
	(384, 'Delhi', 'Delhi'),
	(43, 'Faridabad', 'Haryana'),
	(46, 'Ghaziabad', 'Uttar Pradesh'),
	(22, 'Guntur', 'Andhra Pradesh'),
	(44, 'Gurgaon', 'Haryana'),
	(58, 'Gwalior', 'Madhya Pradesh'),
	(5, 'Hyderabad', 'Telengana'),
	(9, 'Indore', 'Madhya Pradesh'),
	(11, 'Jaipur', 'Rajasthan'),
	(23, 'Jamshedpur', 'Jharkhand'),
	(55, 'Jodhpur', 'Rajasthan'),
	(24, 'Kakinada', 'Andhra Pradesh'),
	(8, 'Kanpur', 'Uttar Pradesh'),
	(53, 'Kolhapur', 'Maharashtra'),
	(383, 'Kolkata', 'West Bengal'),
	(7, 'Lucknow', 'Uttar Pradesh'),
	(59, 'Ludhiana', 'Punjab'),
	(25, 'Madurai', 'Tamil Nadu'),
	(26, 'Mangalore', 'Karnataka'),
	(27, 'Meerut', 'Uttar Pradesh'),
	(60, 'Mohali', 'Punjab'),
	(1, 'Mumbai', 'Maharashtra'),
	(28, 'Mysuru ', 'Karnataka'),
	(38, 'Nagpur', 'Maharashtra'),
	(39, 'Nashik ', 'Maharashtra'),
	(29, 'Nellore', 'Andhra Pradesh'),
	(45, 'Noida', 'Uttar Pradesh'),
	(14, 'Patna', 'Bihar'),
	(2, 'Pune', 'Maharashtra'),
	(15, 'Raipur', 'Chattisgarh'),
	(49, 'Rajahmundry', 'Andhra Pradesh'),
	(31, 'Rajkot', 'Gujarat'),
	(13, 'Ranchi', 'Jharkhand'),
	(32, 'Salem', 'Tamil Nadu'),
	(54, 'Sangli', 'Maharashtra'),
	(52, 'Satara', 'Maharashtra'),
	(57, 'Surat', 'Gujarat'),
	(33, 'Trichy', 'Tamil Nadu'),
	(34, 'Tumkur', 'Karnataka'),
	(36, 'Varanasi', 'Uttar Pradesh'),
	(35, 'Vellore', 'Tamil Nadu'),
	(40, 'Vijayawada ', 'Andhra Pradesh'),
	(41, 'Vizag ', 'Andhra Pradesh');

-- Dumping structure for table caregiversaathi.diseases
DROP TABLE IF EXISTS `diseases`;
CREATE TABLE IF NOT EXISTS `diseases` (
  `disease_id` int NOT NULL AUTO_INCREMENT,
  `disease_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `enabled` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'Y',
  `sort_id` int DEFAULT '1',
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'caregiversaathi.app@gmail.com',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'caregiversaathi.app@gmail.com',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`disease_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.diseases: ~14 rows (approximately)
DELETE FROM `diseases`;
INSERT INTO `diseases` (`disease_id`, `disease_name`, `enabled`, `sort_id`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`) VALUES
	(1, 'General', 'Y', 0, 'info@caregiversaathi.co.in', '2022-03-24 01:05:16', 'info@caregiversaathi.co.in', '2022-03-24 01:05:16'),
	(2, 'Covid', 'Y', 1, 'info@caregiversaathi.co.in', '2022-03-24 01:20:48', 'info@caregiversaathi.co.in', '2022-03-24 01:20:48'),
	(3, 'ALS', 'Y', 1, 'info@caregiversaathi.co.in', '2022-04-08 21:07:47', 'info@caregiversaathi.co.in', '2022-04-08 21:07:47'),
	(4, 'Cancer', 'Y', 1, 'info@caregiversaathi.co.in', '2022-04-08 21:08:26', 'info@caregiversaathi.co.in', '2022-04-08 21:08:26'),
	(5, 'Dementia', 'Y', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-07-15 10:47:56', 'info@caregiversaathi.co.in', '2022-07-15 10:47:56'),
	(6, 'MS', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-07-15 10:47:56', 'info@caregiversaathi.co.in', '2022-07-15 10:47:56'),
	(7, 'Bipolar', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-07-15 10:47:56', 'info@caregiversaathi.co.in', '2022-07-15 10:47:56'),
	(8, 'Alzheimer\'s', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07'),
	(9, 'Clinical depression', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07'),
	(10, 'Renal Disease', 'Y', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07'),
	(11, 'Multiple System Atrophy', 'Y', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07'),
	(12, 'Epilepsy', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07'),
	(13, 'Schizophrenia', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07'),
	(14, 'Autism', 'N', 1, 'ria.chakravorty@caregiversaathi.co.in', '2022-08-08 12:27:07', 'info@caregiversaathi.co.in', '2022-08-08 12:27:07');

-- Dumping structure for table caregiversaathi.languages
DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `language_id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `language_name` varchar(100) NOT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `language_name` (`language_name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.languages: ~13 rows (approximately)
DELETE FROM `languages`;
INSERT INTO `languages` (`language_id`, `language_name`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`) VALUES
	(1, 'English', 'info@caregiversaathi.co.in', '2022-04-08 21:16:18', 'info@caregiversaathi.co.in', '2022-04-08 21:16:18'),
	(2, 'Hindi', 'info@caregiversaathi.co.in', '2022-04-08 21:16:22', 'info@caregiversaathi.co.in', '2022-04-08 21:16:22'),
	(3, 'Marathi', 'info@caregiversaathi.co.in', '2022-04-08 21:16:27', 'info@caregiversaathi.co.in', '2022-04-08 21:16:27'),
	(4, 'Bengali', 'info@caregiversaathi.co.in', '2022-04-10 17:39:51', 'info@caregiversaathi.co.in', '2022-04-10 17:39:51'),
	(5, 'Telugu', 'info@caregiversaathi.co.in', '2022-04-10 17:40:08', 'info@caregiversaathi.co.in', '2022-04-10 17:40:08'),
	(6, 'Tamil', 'info@caregiversaathi.co.in', '2022-04-10 17:40:21', 'info@caregiversaathi.co.in', '2022-04-10 17:40:21'),
	(7, 'Urdu', 'info@caregiversaathi.co.in', '2022-04-10 17:40:31', 'info@caregiversaathi.co.in', '2022-04-10 17:40:31'),
	(8, 'Gujarati', 'info@caregiversaathi.co.in', '2022-04-10 17:40:38', 'info@caregiversaathi.co.in', '2022-04-10 17:40:38'),
	(9, 'Kannada', 'info@caregiversaathi.co.in', '2022-04-10 17:40:53', 'info@caregiversaathi.co.in', '2022-04-10 17:40:53'),
	(10, 'Malayalam', 'info@caregiversaathi.co.in', '2022-04-10 17:41:09', 'info@caregiversaathi.co.in', '2022-04-10 17:41:09'),
	(11, 'Oriya', 'info@caregiversaathi.co.in', '2022-04-10 17:41:18', 'info@caregiversaathi.co.in', '2022-04-10 17:41:18'),
	(12, 'Punjabi', 'info@caregiversaathi.co.in', '2022-04-10 17:41:30', 'info@caregiversaathi.co.in', '2022-04-10 17:41:30'),
	(13, 'Konkani', 'info@caregiversaathi.co.in', '2022-04-10 17:42:17', 'info@caregiversaathi.co.in', '2022-04-10 17:42:17');

-- Dumping structure for table caregiversaathi.performance
DROP TABLE IF EXISTS `performance`;
CREATE TABLE IF NOT EXISTS `performance` (
  `performance_id` int NOT NULL AUTO_INCREMENT,
  `unverified_leads_id` int NOT NULL,
  `disease_id` int NOT NULL,
  `service_id` int NOT NULL,
  `service_attribute_id` int unsigned DEFAULT NULL,
  `vendor_id` int unsigned NOT NULL,
  `stock` varchar(50) DEFAULT NULL,
  `create_ui` varchar(50) NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `volunteer_id` varchar(50) DEFAULT 'info@caregiversaathi.co.in',
  `volunteer_remarks` varchar(4000) DEFAULT NULL,
  `volunteer_leads_verification_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `admin_remarks` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `admin_verification_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_approved` char(2) DEFAULT 'P',
  PRIMARY KEY (`performance_id`),
  KEY `FK_performance_users` (`volunteer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.performance: ~71 rows (approximately)
DELETE FROM `performance`;
INSERT INTO `performance` (`performance_id`, `unverified_leads_id`, `disease_id`, `service_id`, `service_attribute_id`, `vendor_id`, `stock`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`, `volunteer_id`, `volunteer_remarks`, `volunteer_leads_verification_ts`, `admin_remarks`, `admin_verification_ts`, `is_approved`) VALUES
	(30, 4, 2, 1, 2, 17, '10', 'info@caregiversaathi.co.in', '2022-09-16 05:59:30', 'info@caregiversaathi.co.in', '2022-10-12 14:14:52', 'info@caregiversaathi.co.in', 'Verified', '2022-09-16 05:59:30', NULL, '2022-10-12 14:14:52', 'N'),
	(31, 5, 2, 1, 1, 17, '10', 'info@caregiversaathi.co.in', '2022-09-16 05:59:30', 'info@caregiversaathi.co.in', '2022-09-17 10:43:06', 'info@caregiversaathi.co.in', 'Verified', '2022-09-16 05:59:30', NULL, '2022-09-17 10:43:06', 'Y'),
	(32, 6, 1, 1, 2, 18, '20', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-16 07:55:13', 'info@caregiversaathi.co.in', '2022-09-16 10:53:23', 'minakshy.iyer@caregiversaathi.co.in', '', '2022-09-16 07:55:13', NULL, '2022-09-16 10:53:23', 'Y'),
	(33, 7, 1, 1, 1, 18, '50', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-16 07:55:13', 'info@caregiversaathi.co.in', '2022-09-17 09:53:37', 'minakshy.iyer@caregiversaathi.co.in', '', '2022-09-16 07:55:13', NULL, '2022-09-17 09:53:37', 'Y'),
	(34, 8, 1, 2, 3, 16, '10', 'info@caregiversaathi.co.in', '2022-09-16 10:25:43', 'info@caregiversaathi.co.in', '2022-09-17 09:57:04', 'info@caregiversaathi.co.in', '', '2022-09-16 10:25:43', NULL, '2022-09-17 09:57:04', 'Y'),
	(35, 9, 1, 6, 13, 16, '0', 'info@caregiversaathi.co.in', '2022-09-16 10:26:06', 'info@caregiversaathi.co.in', '2022-09-16 10:26:06', 'info@caregiversaathi.co.in', '', '2022-09-16 10:26:06', NULL, '2022-09-16 10:26:05', 'P'),
	(36, 10, 1, 6, 12, 16, '30', 'info@caregiversaathi.co.in', '2022-09-16 10:26:06', 'info@caregiversaathi.co.in', '2022-09-16 10:26:06', 'info@caregiversaathi.co.in', '', '2022-09-16 10:26:06', NULL, '2022-09-16 10:26:05', 'P'),
	(37, 11, 1, 1, 2, 19, '10', 'eknabiya@gmail.com', '2022-09-17 09:49:01', 'info@caregiversaathi.co.in', '2022-09-17 09:51:58', 'eknabiya@gmail.com', 'Bed number can vary ', '2022-09-17 09:49:01', NULL, '2022-09-17 09:51:58', 'Y'),
	(38, 12, 1, 1, 1, 19, '20', 'eknabiya@gmail.com', '2022-09-17 09:49:01', 'info@caregiversaathi.co.in', '2022-09-17 09:52:06', 'eknabiya@gmail.com', 'Bed number can vary ', '2022-09-17 09:49:01', NULL, '2022-09-17 09:52:06', 'Y'),
	(39, 13, 1, 1, 2, 22, 'Yes', 'vimala.mv@caregiversaathi.co.in', '2022-09-17 09:58:14', 'info@caregiversaathi.co.in', '2022-09-17 09:58:54', 'vimala.mv@caregiversaathi.co.in', '', '2022-09-17 09:58:14', NULL, '2022-09-17 09:58:54', 'Y'),
	(40, 14, 1, 1, 1, 22, 'Yes', 'vimala.mv@caregiversaathi.co.in', '2022-09-17 09:58:14', 'info@caregiversaathi.co.in', '2022-09-17 10:00:02', 'vimala.mv@caregiversaathi.co.in', '', '2022-09-17 09:58:14', NULL, '2022-09-17 10:00:02', 'Y'),
	(41, 15, 1, 8, 15, 23, '0', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 10:00:16', 'info@caregiversaathi.co.in', '2022-09-17 10:00:38', 'minakshy.iyer@caregiversaathi.co.in', '', '2022-09-17 10:00:16', NULL, '2022-09-17 10:00:38', 'Y'),
	(42, 24, 1, 3, 4, 17, '8', 'info@caregiversaathi.co.in', '2022-10-12 06:29:38', 'info@caregiversaathi.co.in', '2022-10-12 14:14:58', 'info@caregiversaathi.co.in', '', '2022-10-12 06:29:38', NULL, '2022-10-12 14:14:58', 'N'),
	(43, 25, 1, 1, 2, 52, '2', 'info@caregiversaathi.co.in', '2022-10-12 06:43:24', 'info@caregiversaathi.co.in', '2022-10-12 09:50:47', 'info@caregiversaathi.co.in', '', '2022-10-12 06:43:24', NULL, '2022-10-12 09:50:47', 'N'),
	(44, 26, 1, 1, 1, 52, '4', 'info@caregiversaathi.co.in', '2022-10-12 06:43:24', 'info@caregiversaathi.co.in', '2022-10-12 09:50:51', 'info@caregiversaathi.co.in', '', '2022-10-12 06:43:24', NULL, '2022-10-12 09:50:51', 'N'),
	(45, 27, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:47:28', 'nandukrishnanka@gmail.com', '2022-10-12 09:47:28', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:47:28', NULL, '2022-10-12 09:47:28', 'NA'),
	(46, 28, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:47:28', 'nandukrishnanka@gmail.com', '2022-10-12 09:47:28', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:47:28', NULL, '2022-10-12 09:47:28', 'NA'),
	(47, 27, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:48:22', 'info@caregiversaathi.co.in', '2022-10-12 09:50:32', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:48:22', NULL, '2022-10-12 09:50:32', 'Y'),
	(48, 28, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:48:22', 'info@caregiversaathi.co.in', '2022-10-12 09:50:40', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:48:22', NULL, '2022-10-12 09:50:40', 'Y'),
	(49, 29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:51:27', 'nandukrishnanka@gmail.com', '2022-10-12 09:51:27', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:51:27', NULL, '2022-10-12 09:51:26', 'P'),
	(50, 30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:51:27', 'nandukrishnanka@gmail.com', '2022-10-12 09:51:27', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:51:27', NULL, '2022-10-12 09:51:26', 'P'),
	(51, 29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:52:50', 'nandukrishnanka@gmail.com', '2022-10-12 09:52:50', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:52:50', NULL, '2022-10-12 09:52:49', 'P'),
	(52, 30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:52:50', 'nandukrishnanka@gmail.com', '2022-10-12 09:52:50', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:52:50', NULL, '2022-10-12 09:52:49', 'P'),
	(53, 29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:53:29', 'nandukrishnanka@gmail.com', '2022-10-12 09:53:29', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:53:29', NULL, '2022-10-12 09:53:29', 'P'),
	(54, 30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:53:29', 'nandukrishnanka@gmail.com', '2022-10-12 09:53:29', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:53:29', NULL, '2022-10-12 09:53:29', 'P'),
	(55, 29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:07', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:07', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:56:07', NULL, '2022-10-12 09:56:06', 'P'),
	(56, 30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:07', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:07', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:56:07', NULL, '2022-10-12 09:56:06', 'P'),
	(57, 29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:45', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:45', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:56:45', NULL, '2022-10-12 09:56:44', 'P'),
	(58, 30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:45', 'nandukrishnanka@gmail.com', '2022-10-12 09:56:45', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:56:45', NULL, '2022-10-12 09:56:44', 'P'),
	(59, 29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:57:21', 'nandukrishnanka@gmail.com', '2022-10-12 09:57:21', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:57:21', NULL, '2022-10-12 09:57:20', 'P'),
	(60, 30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:57:21', 'nandukrishnanka@gmail.com', '2022-10-12 09:57:21', 'nandukrishnanka@gmail.com', 'NA', '2022-10-12 09:57:21', NULL, '2022-10-12 09:57:20', 'P'),
	(61, 31, 1, 1, 2, 54, '20', 'napiersandra285@gmail.com', '2022-10-12 11:54:34', 'info@caregiversaathi.co.in', '2022-10-12 12:11:37', 'napiersandra285@gmail.com', 'Open 24 hours for accidents and emergencies', '2022-10-12 11:54:34', NULL, '2022-10-12 12:11:37', 'Y'),
	(62, 32, 1, 1, 1, 54, '125', 'napiersandra285@gmail.com', '2022-10-12 11:54:34', 'info@caregiversaathi.co.in', '2022-10-12 12:11:43', 'napiersandra285@gmail.com', 'Open 24 hours for accidents and emergencies', '2022-10-12 11:54:34', NULL, '2022-10-12 12:11:43', 'Y'),
	(63, 33, 1, 1, 2, 18, '25', 'info@caregiversaathi.co.in', '2022-10-12 14:46:03', 'info@caregiversaathi.co.in', '2022-10-12 14:46:03', 'info@caregiversaathi.co.in', '', '2022-10-12 14:46:03', NULL, '2022-10-12 14:46:03', 'P'),
	(64, 34, 1, 1, 1, 18, '200', 'info@caregiversaathi.co.in', '2022-10-12 14:46:03', 'info@caregiversaathi.co.in', '2022-10-12 14:46:03', 'info@caregiversaathi.co.in', '', '2022-10-12 14:46:03', NULL, '2022-10-12 14:46:03', 'P'),
	(65, 33, 1, 1, 2, 18, '25', 'info@caregiversaathi.co.in', '2022-10-12 14:49:54', 'info@caregiversaathi.co.in', '2022-10-12 14:49:54', 'info@caregiversaathi.co.in', 'Bed numbers may vary', '2022-10-12 14:49:54', NULL, '2022-10-12 14:49:53', 'P'),
	(66, 34, 1, 1, 1, 18, '200', 'info@caregiversaathi.co.in', '2022-10-12 14:49:54', 'info@caregiversaathi.co.in', '2022-10-12 14:49:54', 'info@caregiversaathi.co.in', 'Bed numbers may vary', '2022-10-12 14:49:54', NULL, '2022-10-12 14:49:53', 'P'),
	(67, 35, 1, 1, 2, 55, '10', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', 'Open 24*7', '2022-10-12 15:34:38', NULL, '2022-10-12 15:34:38', 'P'),
	(68, 36, 1, 1, 1, 55, '40', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', 'Open 24*7', '2022-10-12 15:34:38', NULL, '2022-10-12 15:34:38', 'P'),
	(69, 37, 1, 1, 2, 56, '10', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 15:36:22', NULL, '2022-10-12 15:36:22', 'P'),
	(70, 38, 1, 1, 1, 56, '60', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 15:36:22', NULL, '2022-10-12 15:36:22', 'P'),
	(71, 37, 1, 1, 2, 56, '10', 'napiersandra285@gmail.com', '2022-10-12 15:36:30', 'napiersandra285@gmail.com', '2022-10-12 15:36:30', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 15:36:30', NULL, '2022-10-12 15:36:29', 'P'),
	(72, 38, 1, 1, 1, 56, '60', 'napiersandra285@gmail.com', '2022-10-12 15:36:30', 'napiersandra285@gmail.com', '2022-10-12 15:36:30', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 15:36:30', NULL, '2022-10-12 15:36:29', 'P'),
	(73, 39, 1, 1, 2, 57, '10', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', 'Mon - Sun  07:00 AM - 10:00 PM', '2022-10-12 15:40:19', NULL, '2022-10-12 15:40:19', 'P'),
	(74, 40, 1, 1, 1, 57, '80', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', 'Mon - Sun  07:00 AM - 10:00 PM', '2022-10-12 15:40:19', NULL, '2022-10-12 15:40:19', 'P'),
	(75, 41, 1, 1, 2, 58, '15', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', 'Mon - Sat  10:00 AM - 06:00 PM', '2022-10-12 15:42:12', NULL, '2022-10-12 15:42:11', 'P'),
	(76, 42, 1, 1, 1, 58, '250', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', 'Mon - Sat  10:00 AM - 06:00 PM', '2022-10-12 15:42:12', NULL, '2022-10-12 15:42:11', 'P'),
	(77, 43, 1, 1, 2, 59, '10', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', 'Mon - Sun  09:30 AM - 07:00 PM', '2022-10-12 15:43:56', NULL, '2022-10-12 15:43:56', 'P'),
	(78, 44, 1, 1, 1, 59, '70', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', 'Mon - Sun  09:30 AM - 07:00 PM', '2022-10-12 15:43:56', NULL, '2022-10-12 15:43:56', 'P'),
	(79, 45, 1, 1, 2, 60, '5', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', 'Mon - Sat  08:00 AM - 08:00 PM', '2022-10-12 15:46:14', NULL, '2022-10-12 15:46:14', 'P'),
	(80, 46, 1, 1, 1, 60, '20', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', 'Mon - Sat  08:00 AM - 08:00 PM', '2022-10-12 15:46:14', NULL, '2022-10-12 15:46:14', 'P'),
	(81, 45, 1, 1, 2, 60, '5', 'napiersandra285@gmail.com', '2022-10-12 15:46:19', 'napiersandra285@gmail.com', '2022-10-12 15:46:19', 'napiersandra285@gmail.com', 'Mon - Sat  08:00 AM - 08:00 PM', '2022-10-12 15:46:19', NULL, '2022-10-12 15:46:19', 'P'),
	(82, 46, 1, 1, 1, 60, '20', 'napiersandra285@gmail.com', '2022-10-12 15:46:19', 'napiersandra285@gmail.com', '2022-10-12 15:46:19', 'napiersandra285@gmail.com', 'Mon - Sat  08:00 AM - 08:00 PM', '2022-10-12 15:46:19', NULL, '2022-10-12 15:46:19', 'P'),
	(83, 47, 1, 1, 2, 61, '10', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', 'Mon - Sun  08:00 AM - 10:00 PM', '2022-10-12 15:54:17', NULL, '2022-10-12 15:54:17', 'P'),
	(84, 48, 1, 1, 1, 61, '75', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', 'Mon - Sun  08:00 AM - 10:00 PM', '2022-10-12 15:54:17', NULL, '2022-10-12 15:54:17', 'P'),
	(85, 49, 1, 1, 2, 62, '50', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', 'Mon - Sun  08:00 AM - 08:00 PM', '2022-10-12 15:58:05', NULL, '2022-10-12 15:58:04', 'P'),
	(86, 50, 1, 1, 1, 62, '400', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', 'Mon - Sun  08:00 AM - 08:00 PM', '2022-10-12 15:58:05', NULL, '2022-10-12 15:58:04', 'P'),
	(87, 51, 1, 1, 2, 63, '10', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 16:21:43', NULL, '2022-10-12 16:21:42', 'P'),
	(88, 52, 1, 1, 1, 63, '30', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 16:21:43', NULL, '2022-10-12 16:21:42', 'P'),
	(89, 51, 1, 1, 2, 63, '10', 'napiersandra285@gmail.com', '2022-10-12 16:22:19', 'napiersandra285@gmail.com', '2022-10-12 16:22:19', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 16:22:19', NULL, '2022-10-12 16:22:19', 'P'),
	(90, 52, 1, 1, 1, 63, '30', 'napiersandra285@gmail.com', '2022-10-12 16:22:19', 'napiersandra285@gmail.com', '2022-10-12 16:22:19', 'napiersandra285@gmail.com', 'Open 24 x 7', '2022-10-12 16:22:19', NULL, '2022-10-12 16:22:19', 'P'),
	(91, 53, 10, 1, 2, 64, '5', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', 'Kidney specialist', '2022-10-12 16:24:47', NULL, '2022-10-12 16:24:46', 'P'),
	(92, 54, 10, 1, 1, 64, '30', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', 'Kidney specialist', '2022-10-12 16:24:47', NULL, '2022-10-12 16:24:46', 'P'),
	(93, 55, 1, 1, 2, 65, '10', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '24*7', '2022-10-12 16:25:50', NULL, '2022-10-12 16:25:49', 'P'),
	(94, 56, 1, 1, 1, 65, '50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '24*7', '2022-10-12 16:25:50', NULL, '2022-10-12 16:25:49', 'P'),
	(95, 57, 1, 1, 2, 66, '5', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', 'open 24*7', '2022-10-12 16:33:48', NULL, '2022-10-12 16:33:47', 'P'),
	(96, 58, 1, 1, 1, 66, '45', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', 'open 24*7', '2022-10-12 16:33:48', NULL, '2022-10-12 16:33:47', 'P'),
	(97, 59, 1, 1, 2, 67, '8', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', 'open 24*7', '2022-10-12 16:35:35', NULL, '2022-10-12 16:35:34', 'P'),
	(98, 60, 1, 1, 1, 67, '50', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', 'open 24*7', '2022-10-12 16:35:35', NULL, '2022-10-12 16:35:34', 'P'),
	(99, 59, 1, 1, 2, 67, '8', 'napiersandra285@gmail.com', '2022-10-12 16:35:40', 'napiersandra285@gmail.com', '2022-10-12 16:35:40', 'napiersandra285@gmail.com', 'open 24*7', '2022-10-12 16:35:40', NULL, '2022-10-12 16:35:40', 'P'),
	(100, 60, 1, 1, 1, 67, '50', 'napiersandra285@gmail.com', '2022-10-12 16:35:40', 'napiersandra285@gmail.com', '2022-10-12 16:35:41', 'napiersandra285@gmail.com', 'open 24*7', '2022-10-12 16:35:41', NULL, '2022-10-12 16:35:40', 'P');

-- Dumping structure for table caregiversaathi.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.roles: ~2 rows (approximately)
DELETE FROM `roles`;
INSERT INTO `roles` (`role_id`, `role_name`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`) VALUES
	(1, 'Admin', 'info@caregiversaathi.co.in', '2022-03-24 17:45:44', 'info@caregiversaathi.co.in', '2022-03-24 17:45:44'),
	(2, 'Volunteer', 'info@caregiversaathi.co.in', '2022-03-24 17:46:47', 'info@caregiversaathi.co.in', '2022-03-24 17:46:47');

-- Dumping structure for table caregiversaathi.services
DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int NOT NULL AUTO_INCREMENT,
  `service_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `enabled` char(50) NOT NULL DEFAULT 'Y',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`service_id`) USING BTREE,
  UNIQUE KEY `service_name` (`service_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.services: ~7 rows (approximately)
DELETE FROM `services`;
INSERT INTO `services` (`service_id`, `service_name`, `enabled`, `remark`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`) VALUES
	(1, 'Hospital Beds', 'Y', NULL, 'info@caregiversaathi.co.in', '2022-03-22 01:38:15', 'info@caregiversaathi.co.in', '2022-03-22 01:38:15'),
	(2, 'Oxygen', 'N', NULL, 'info@caregiversaathi.co.in', '2022-03-22 01:38:15', 'info@caregiversaathi.co.in', '2022-03-22 01:38:15'),
	(3, 'Ambulance', 'Y', NULL, 'info@caregiversaathi.co.in', '2022-03-22 01:38:15', 'info@caregiversaathi.co.in', '2022-03-22 01:38:15'),
	(4, 'Plasma', 'N', NULL, 'info@caregiversaathi.co.in', '2022-03-22 01:38:15', 'info@caregiversaathi.co.in', '2022-03-22 01:38:15'),
	(5, 'Home ICU', 'N', NULL, 'info@caregiversaathi.co.in', '2022-03-22 01:38:15', 'info@caregiversaathi.co.in', '2022-03-22 01:38:15'),
	(8, 'Medical Equipement', 'N', '', 'info@caregiversaathi.co.in', '2022-09-17 09:53:04', 'info@caregiversaathi.co.in', '2022-09-17 09:53:04'),
	(9, 'Diagnostic Centres', 'Y', '', 'info@caregiversaathi.co.in', '2022-10-10 06:03:27', 'info@caregiversaathi.co.in', '2022-10-10 06:03:27');

-- Dumping structure for table caregiversaathi.service_attributes
DROP TABLE IF EXISTS `service_attributes`;
CREATE TABLE IF NOT EXISTS `service_attributes` (
  `service_attribute_id` int unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int NOT NULL DEFAULT '0',
  `service_attribute_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`service_attribute_id`) USING BTREE,
  UNIQUE KEY `service_id_service_attribute_name` (`service_id`,`service_attribute_name`),
  KEY `FK_service_attributes_services` (`service_id`),
  CONSTRAINT `FK_service_attributes_services` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.service_attributes: ~9 rows (approximately)
DELETE FROM `service_attributes`;
INSERT INTO `service_attributes` (`service_attribute_id`, `service_id`, `service_attribute_name`) VALUES
	(2, 1, 'ICU Beds'),
	(1, 1, 'Normal Beds'),
	(3, 2, 'Oxygen Cyclinders'),
	(4, 3, 'No of Ambulances'),
	(10, 4, 'No of Plasma'),
	(11, 5, 'No of Beds'),
	(15, 8, 'Home ICU'),
	(16, 9, 'Covid '),
	(17, 9, 'General ');

-- Dumping structure for table caregiversaathi.unverified_leads
DROP TABLE IF EXISTS `unverified_leads`;
CREATE TABLE IF NOT EXISTS `unverified_leads` (
  `unverified_leads_id` int NOT NULL AUTO_INCREMENT,
  `disease_id` int NOT NULL,
  `service_id` int NOT NULL,
  `service_attribute_id` int unsigned DEFAULT NULL,
  `vendor_id` int unsigned NOT NULL,
  `stock` varchar(50) DEFAULT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `volunteer_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `city_id` int unsigned DEFAULT NULL,
  `address` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `pincode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `contact_no` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `volunteer_remarks` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `volunteer_leads_verification_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`unverified_leads_id`) USING BTREE,
  KEY `FK_unverified_leads_diseases` (`disease_id`),
  KEY `FK_leads_users_2` (`volunteer_id`),
  KEY `FK_unverified_leads_vendor` (`vendor_id`),
  KEY `service_id` (`service_id`),
  KEY `FK_unverified_leads_cities` (`city_id`),
  CONSTRAINT `FK_unverified_leads_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`),
  CONSTRAINT `FK_unverified_leads_diseases` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`disease_id`),
  CONSTRAINT `FK_unverified_leads_services` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
  CONSTRAINT `FK_unverified_leads_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table caregiversaathi.unverified_leads: ~38 rows (approximately)
DELETE FROM `unverified_leads`;
INSERT INTO `unverified_leads` (`unverified_leads_id`, `disease_id`, `service_id`, `service_attribute_id`, `vendor_id`, `stock`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`, `volunteer_id`, `city_id`, `address`, `pincode`, `contact_no`, `volunteer_remarks`, `volunteer_leads_verification_ts`) VALUES
	(16, 1, 1, NULL, 35, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:31:42', 'napiersandra285@gmail.com', '2022-10-11 16:31:42', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:31:42'),
	(17, 1, 1, NULL, 36, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:32:08', 'napiersandra285@gmail.com', '2022-10-11 16:32:08', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:32:08'),
	(18, 1, 1, NULL, 37, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:32:17', 'napiersandra285@gmail.com', '2022-10-11 16:32:17', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:32:17'),
	(19, 1, 1, NULL, 38, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:32:18', 'napiersandra285@gmail.com', '2022-10-11 16:32:18', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:32:18'),
	(20, 1, 1, NULL, 39, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:32:24', 'napiersandra285@gmail.com', '2022-10-11 16:32:24', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:32:24'),
	(21, 1, 1, NULL, 40, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:34:34', 'napiersandra285@gmail.com', '2022-10-11 16:34:34', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:34:34'),
	(22, 1, 1, NULL, 41, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:34:34', 'napiersandra285@gmail.com', '2022-10-11 16:34:34', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:34:34'),
	(23, 1, 1, NULL, 42, NULL, 'napiersandra285@gmail.com', '2022-10-11 16:34:35', 'napiersandra285@gmail.com', '2022-10-11 16:34:35', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', '', '2022-10-11 16:34:35'),
	(29, 1, 1, 2, 53, '20', 'nandukrishnanka@gmail.com', '2022-10-12 09:51:27', 'nandukrishnanka@gmail.com', '2022-10-12 09:57:21', 'nandukrishnanka@gmail.com', 1, 'Ayyappa Rd, Dr D Y Patil Vidyanagar, Sector 5, Nerul, Navi Mumbai, Maharashtra 400706', '400706', '912227736901', 'NA', '2022-10-12 09:57:21'),
	(30, 1, 1, 1, 53, '100', 'nandukrishnanka@gmail.com', '2022-10-12 09:51:27', 'nandukrishnanka@gmail.com', '2022-10-12 09:57:21', 'nandukrishnanka@gmail.com', 1, 'Ayyappa Rd, Dr D Y Patil Vidyanagar, Sector 5, Nerul, Navi Mumbai, Maharashtra 400706', '400706', '912227736901', 'NA', '2022-10-12 09:57:21'),
	(33, 1, 1, 2, 18, '25', 'info@caregiversaathi.co.in', '2022-10-12 14:46:03', 'info@caregiversaathi.co.in', '2022-10-12 14:49:54', 'info@caregiversaathi.co.in', 383, 'Block - A, Scheme -L11, P -4 & 5, Gariahaat Road , Dhakuria', '700029', '03366800000', 'Bed numbers may vary', '2022-10-12 14:49:54'),
	(34, 1, 1, 1, 18, '200', 'info@caregiversaathi.co.in', '2022-10-12 14:46:03', 'info@caregiversaathi.co.in', '2022-10-12 14:49:54', 'info@caregiversaathi.co.in', 383, 'Block - A, Scheme -L11, P -4 & 5, Gariahaat Road , Dhakuria', '700029', '03366800000', 'Bed numbers may vary', '2022-10-12 14:49:54'),
	(35, 1, 1, 2, 55, '10', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', 4, '98, Hosa Rd, opposite old rto office, Naganathapura, Rayasandra, Bengaluru, Karnataka 560100', '560100', '917892408186', 'Open 24*7', '2022-10-12 15:34:38'),
	(36, 1, 1, 1, 55, '40', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', 4, '98, Hosa Rd, opposite old rto office, Naganathapura, Rayasandra, Bengaluru, Karnataka 560100', '560100', '917892408186', 'Open 24*7', '2022-10-12 15:34:38'),
	(37, 1, 1, 2, 56, '10', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', '2022-10-12 15:36:30', 'napiersandra285@gmail.com', 4, '187/269, Dr Puneeth Rajkumar Rd, Agara Village, 1st Sector, HSR Layout, Bengaluru, Karnataka 560102', '560102', '917899674400', 'Open 24 x 7', '2022-10-12 15:36:30'),
	(38, 1, 1, 1, 56, '60', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', '2022-10-12 15:36:30', 'napiersandra285@gmail.com', 4, '187/269, Dr Puneeth Rajkumar Rd, Agara Village, 1st Sector, HSR Layout, Bengaluru, Karnataka 560102', '560102', '917899674400', 'Open 24 x 7', '2022-10-12 15:36:30'),
	(39, 1, 1, 2, 57, '10', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', 4, '98, HAL Old Airport Rd, Kodihalli, Bengaluru, Karnataka 560017', '560017', '918025023200', 'Mon - Sun  07:00 AM - 10:00 PM', '2022-10-12 15:40:19'),
	(40, 1, 1, 1, 57, '80', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', 4, '98, HAL Old Airport Rd, Kodihalli, Bengaluru, Karnataka 560017', '560017', '918025023200', 'Mon - Sun  07:00 AM - 10:00 PM', '2022-10-12 15:40:19'),
	(41, 1, 1, 2, 58, '15', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', 4, '67, Uttarahalli Main Rd, Sunkalpalya, Bengaluru, Karnataka 560060', '560060', '918026255555', 'Mon - Sat  10:00 AM - 06:00 PM', '2022-10-12 15:42:12'),
	(42, 1, 1, 1, 58, '250', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', 4, '67, Uttarahalli Main Rd, Sunkalpalya, Bengaluru, Karnataka 560060', '560060', '918026255555', 'Mon - Sat  10:00 AM - 06:00 PM', '2022-10-12 15:42:12'),
	(43, 1, 1, 2, 59, '10', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', 4, '5, Langford Gardens, Bengaluru, Karnataka 560025', '560025', '918041245450', 'Mon - Sun  09:30 AM - 07:00 PM', '2022-10-12 15:43:56'),
	(44, 1, 1, 1, 59, '70', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', 4, '5, Langford Gardens, Bengaluru, Karnataka 560025', '560025', '918041245450', 'Mon - Sun  09:30 AM - 07:00 PM', '2022-10-12 15:43:56'),
	(45, 1, 1, 2, 60, '5', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', '2022-10-12 15:46:19', 'napiersandra285@gmail.com', 4, 'SY NO 52/2 & 52/3, Devarabeesanahalli, Varthur Hobli Opp Intel, Dr Puneeth Rajkumar Rd, Marathahalli, Bengaluru, Karnataka 560103', '560103', '918049694969', 'Mon - Sat  08:00 AM - 08:00 PM', '2022-10-12 15:46:19'),
	(46, 1, 1, 1, 60, '20', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', '2022-10-12 15:46:19', 'napiersandra285@gmail.com', 4, 'SY NO 52/2 & 52/3, Devarabeesanahalli, Varthur Hobli Opp Intel, Dr Puneeth Rajkumar Rd, Marathahalli, Bengaluru, Karnataka 560103', '560103', '918049694969', 'Mon - Sat  08:00 AM - 08:00 PM', '2022-10-12 15:46:19'),
	(47, 1, 1, 2, 61, '10', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', 4, '23, 80 Feet Road, Guru Krupa Layout, 2nd Stage', '560072', '918023014117', 'Mon - Sun  08:00 AM - 10:00 PM', '2022-10-12 15:54:17'),
	(48, 1, 1, 1, 61, '75', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', 4, '23, 80 Feet Road, Guru Krupa Layout, 2nd Stage', '560072', '918023014117', 'Mon - Sun  08:00 AM - 10:00 PM', '2022-10-12 15:54:17'),
	(49, 1, 1, 2, 62, '50', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', 4, '154 9, Bannerghatta Main Rd, opposite IIM, Sahyadri Layout, Bilekahalli, Bengaluru, Karnataka 560076', '560076', '09663367253', 'Mon - Sun  08:00 AM - 08:00 PM', '2022-10-12 15:58:05'),
	(50, 1, 1, 1, 62, '400', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', '2022-10-12 15:58:05', 'napiersandra285@gmail.com', 4, '154 9, Bannerghatta Main Rd, opposite IIM, Sahyadri Layout, Bilekahalli, Bengaluru, Karnataka 560076', '560076', '09663367253', 'Mon - Sun  08:00 AM - 08:00 PM', '2022-10-12 15:58:05'),
	(51, 1, 1, 2, 63, '10', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', '2022-10-12 16:22:19', 'napiersandra285@gmail.com', 4, 'Ideal Homes Township, Kenchenahalli, Rajarajeshwarinagar, Bengaluru 560098', '560098', '919606980209', 'Open 24 x 7', '2022-10-12 16:22:19'),
	(52, 1, 1, 1, 63, '30', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', '2022-10-12 16:22:19', 'napiersandra285@gmail.com', 4, 'Ideal Homes Township, Kenchenahalli, Rajarajeshwarinagar, Bengaluru 560098', '560098', '919606980209', 'Open 24 x 7', '2022-10-12 16:22:19'),
	(53, 10, 1, 2, 64, '5', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', 4, '3JMM+84J, Balaji Layout, Chokkanahalli, Bengaluru, Karnataka 560064', '560064', '918884048888', 'Kidney specialist', '2022-10-12 16:24:47'),
	(54, 10, 1, 1, 64, '30', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', 4, '3JMM+84J, Balaji Layout, Chokkanahalli, Bengaluru, Karnataka 560064', '560064', '918884048888', 'Kidney specialist', '2022-10-12 16:24:47'),
	(55, 1, 1, 2, 65, '10', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', 4, 'Garvebhavi playa, 42 SRVS School, 1st Main Rd, Munireddy Layout, Garvebhavi Palya, Bengaluru, Karnataka 560068', '560068', '919148757717', '24*7', '2022-10-12 16:25:50'),
	(56, 1, 1, 1, 65, '50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', 4, 'Garvebhavi playa, 42 SRVS School, 1st Main Rd, Munireddy Layout, Garvebhavi Palya, Bengaluru, Karnataka 560068', '560068', '919148757717', '24*7', '2022-10-12 16:25:50'),
	(57, 1, 1, 2, 66, '5', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'open 24*7', '2022-10-12 16:33:48'),
	(58, 1, 1, 1, 66, '45', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'open 24*7', '2022-10-12 16:33:48'),
	(59, 1, 1, 2, 67, '8', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', '2022-10-12 16:35:40', 'napiersandra285@gmail.com', 4, 'Ferns City Road, Ferns City, Doddanekundi, Doddanekkundi, Bengaluru, Karnataka 560048', '560048', '919480208666', 'open 24*7', '2022-10-12 16:35:40'),
	(60, 1, 1, 1, 67, '50', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', '2022-10-12 16:35:41', 'napiersandra285@gmail.com', 4, 'Ferns City Road, Ferns City, Doddanekundi, Doddanekkundi, Bengaluru, Karnataka 560048', '560048', '919480208666', 'open 24*7', '2022-10-12 16:35:41');

-- Dumping structure for table caregiversaathi.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `email_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `contact_no` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role_id` tinyint unsigned NOT NULL DEFAULT '2',
  `age` int unsigned DEFAULT '0',
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `city_id` int unsigned NOT NULL,
  `language_id` varchar(200) NOT NULL,
  `time_contribution_frequency` varchar(25) DEFAULT NULL,
  `time_contribution_hours` int DEFAULT NULL,
  `orientation` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'Y',
  `work_area_others` varchar(100) DEFAULT '',
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_approved` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'N',
  `approved_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`email_id`) USING BTREE,
  KEY `FK_users_role` (`role_id`),
  KEY `FK_users_cities` (`city_id`),
  CONSTRAINT `FK_users_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_users_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.users: ~8 rows (approximately)
DELETE FROM `users`;
INSERT INTO `users` (`email_id`, `password`, `first_name`, `last_name`, `contact_no`, `role_id`, `age`, `create_ui`, `create_ts`, `city_id`, `language_id`, `time_contribution_frequency`, `time_contribution_hours`, `orientation`, `work_area_others`, `last_update_ui`, `last_update_ts`, `is_approved`, `approved_by`) VALUES
	('bj20031@astra.xlri.ac.in', 'SY9YrH/a5Jsy6aQLQMTCSw==', 'Nandu', 'Krishnan A', '7012980568', 2, 27, 'bj20031@astra.xlri.ac.in', '2022-10-09 16:14:28', 3, '1,2,10,6', 'hourly', 1, 'Yes', '', 'info@caregiversaathi.co.in', '2022-10-09 16:14:27', 'Y', 'info@caregiversaathi.co.in'),
	('chakravorty.ria@gmail.com', 'GJIPR26NixwmDvDSLD8RMQ==', 'Ria', 'Chak', '9874897589', 2, 35, 'chakravorty.ria@gmail.com', '2022-09-02 10:39:44', 383, '1', 'hourly', 13, 'Yes', '', 'info@caregiversaathi.co.in', '2022-09-02 10:39:43', 'Y', 'info@caregiversaathi.co.in'),
	('eknabiya@gmail.com', 'eQFViV/hOXf+KJ5qZJ2rrA==', 'Nabiya', 'Ethiraj', '7845919775', 2, 35, 'eknabiya@gmail.com', '2022-09-17 09:40:58', 3, '1', 'weekly', 4, 'Yes', '', 'info@caregiversaathi.co.in', '2022-09-17 09:40:58', 'Y', 'info@caregiversaathi.co.in'),
	('info@caregiversaathi.co.in', 'otOSBXVn2QuKWx6viQMyfuQxHZN/pBzV', 'CaregiverSaathi', 'Admin', '', 1, 0, 'info@caregiversaathi.co.in', '2022-08-16 13:11:08', 1, '', NULL, NULL, 'Yes', '', 'info@caregiversaathi.co.in', '2022-08-16 13:11:08', 'Y', 'info@caregiversaathi.co.in'),
	('minakshy.iyer@caregiversaathi.co.in', 'cYZiDxkPHrVYwdO6xlGrZA==', 'Minakshy', 'Iyer', '9831010807', 2, 30, 'minakshy.iyer@caregiversaathi.co.in', '2022-09-16 06:03:02', 4, '1', 'hourly', 1, 'Yes', '', 'info@caregiversaathi.co.in', '2022-09-16 06:03:01', 'Y', 'info@caregiversaathi.co.in'),
	('nandukrishnanka@gmail.com', 'Vy8RtU1Av/6JQFCN/VmERw==', 'Nandu', 'Krishnan A', '7012980568', 2, 27, 'nandukrishnanka@gmail.com', '2022-10-12 07:58:05', 3, '1', 'hourly', 1, 'Yes', '', 'info@caregiversaathi.co.in', '2022-10-12 07:58:05', 'Y', 'info@caregiversaathi.co.in'),
	('napiersandra285@gmail.com', 'g0LauJgDQdcC+KYqXUo8BQ==', 'Sandra', 'Napier', '9074556533', 2, 20, 'napiersandra285@gmail.com', '2022-10-09 16:03:35', 1, '1,2,10', 'weekly', 12, 'Yes', '', 'info@caregiversaathi.co.in', '2022-10-09 16:03:34', 'Y', 'info@caregiversaathi.co.in'),
	('vimala.mv@caregiversaathi.co.in', 'AMXvq90137c=', 'Vimala ', 'Mv', '9886687912', 2, 30, 'vimala.mv@caregiversaathi.co.in', '2022-09-17 09:48:18', 4, '1', 'hourly', 1, 'No', '', 'info@caregiversaathi.co.in', '2022-09-17 09:48:18', 'Y', 'info@caregiversaathi.co.in');

-- Dumping structure for table caregiversaathi.vendor
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE IF NOT EXISTS `vendor` (
  `vendor_id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(100) DEFAULT NULL,
  `city_id` int unsigned NOT NULL DEFAULT '0',
  `address` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `pincode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `contact_no` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`vendor_id`),
  KEY `FK_vendor_cities` (`city_id`),
  CONSTRAINT `FK_vendor_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.vendor: ~55 rows (approximately)
DELETE FROM `vendor`;
INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `city_id`, `address`, `pincode`, `contact_no`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`) VALUES
	(13, 'S S Traders', 42, '19, E /11, Kamla Nehru Rd, near patanjali stores, Civil Lines', '211001', '+919517111371', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10'),
	(14, 'SPLV medical agencies', 49, 'Yeleshwaram', '533429', '+919849227067', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10'),
	(15, 'AAR AAR Health Care', 384, '18-Kailash East, Greater Kailash', '110062', '+919803300008', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10'),
	(16, 'Albino Life Sciences Pvt Ltd', 384, 'B-167,Ground Floor, Kailash East', '110062', '+919810033890', 'info@caregiversaathi.co.in', '2022-08-16 06:08:10', 'info@caregiversaathi.co.in', '2022-09-17 09:57:04'),
	(17, 'ABC ', 383, '184 B CIT Road', '700004', '9831010908', 'info@caregiversaathi.co.in', '2022-09-16 05:59:30', 'info@caregiversaathi.co.in', '2022-09-17 10:43:06'),
	(18, 'AMRI Hospital', 383, 'Block - A, Scheme -L11, P -4 & 5, Gariahaat Road , Dhakuria', '700029', '03366800000', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-16 07:55:13', 'minakshy.iyer@caregiversaathi.co.in', '2022-10-12 14:50:32'),
	(19, 'TMH', 1, 'Parel', '400014', '9869724233', 'eknabiya@gmail.com', '2022-09-17 09:49:01', 'eknabiya@gmail.com', '2022-09-17 09:52:06'),
	(20, 'No Vendor available', 4, '', '', '', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 09:57:46', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 09:57:46'),
	(21, 'No Vendor available', 4, '', '', '', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 09:58:00', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 09:58:00'),
	(22, 'Lilavati ', 1, 'A - 791, A - 791, Bandra reclamation road, general arunkumar vaidya nagar, bandra West, Mumbai ', '400050', '02226751000', 'vimala.mv@caregiversaathi.co.in', '2022-09-17 09:58:14', 'vimala.mv@caregiversaathi.co.in', '2022-09-17 10:00:02'),
	(23, 'Olive', 1, '', '', '55645473', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 10:00:16', 'minakshy.iyer@caregiversaathi.co.in', '2022-09-17 10:00:38'),
	(24, 'Mallige Hospital', 4, '31/32, Crescent Rd, Madhava Nagar, Gandhi Nagar, Bengaluru, Karnataka 560001', '560001', '919448077253', 'napiersandra285@gmail.com', '2022-10-11 16:17:40', 'napiersandra285@gmail.com', '2022-10-11 16:17:40'),
	(25, 'Aaxis Hospital', 4, 'Bellathur Village, Bidarhalli, Whitefield Main Road, Uttarahalli Hobli, Bengaluru, Karnataka 560067', '560067', '917090000384', 'napiersandra285@gmail.com', '2022-10-11 16:18:45', 'napiersandra285@gmail.com', '2022-10-11 16:18:45'),
	(26, 'Blossom Multispecialty Hospital', 4, '98, Hosa Rd, opposite old rto office, Naganathapura, Rayasandra, Bengaluru, Karnataka 560100', '560100', '917892408186', 'napiersandra285@gmail.com', '2022-10-11 16:21:29', 'napiersandra285@gmail.com', '2022-10-11 16:21:29'),
	(27, 'Sai Thunga Multispeciality', 4, '187/269, Dr Puneeth Rajkumar Rd, Agara Village, 1st Sector, HSR Layout, Bengaluru, Karnataka 560102', '560102', '917899674400', 'napiersandra285@gmail.com', '2022-10-11 16:23:25', 'napiersandra285@gmail.com', '2022-10-11 16:23:25'),
	(28, 'Manipal Hospital - HAL', 4, '98, HAL Old Airport Rd, Kodihalli, Bengaluru, Karnataka 560017', '560017', '918025023200', 'napiersandra285@gmail.com', '2022-10-11 16:24:09', 'napiersandra285@gmail.com', '2022-10-11 16:24:09'),
	(29, 'BGS Gleneagle Global Hospital', 4, '67, Uttarahalli Main Rd, Sunkalpalya, Bengaluru, Karnataka 560060', '560060', '918026255555', 'napiersandra285@gmail.com', '2022-10-11 16:25:28', 'napiersandra285@gmail.com', '2022-10-11 16:25:28'),
	(30, 'Republic Hospital', 4, '5, Langford Gardens, Bengaluru, Karnataka 560025', '560025', '918041245450', 'napiersandra285@gmail.com', '2022-10-11 16:26:16', 'napiersandra285@gmail.com', '2022-10-11 16:26:16'),
	(31, 'Sakra ', 4, 'SY NO 52/2 & 52/3, Devarabeesanahalli, Varthur Hobli Opp Intel, Dr Puneeth Rajkumar Rd, Marathahalli, Bengaluru, Karnataka 560103', '560103', '918049694969', 'napiersandra285@gmail.com', '2022-10-11 16:27:01', 'napiersandra285@gmail.com', '2022-10-11 16:27:01'),
	(32, 'Revive Hospital', 4, '190, Paramahansa Yogananda Rd, opposite Cauvery, Stage 2, Eshwara Layout, Indiranagar, Bengaluru, Karnataka 560038', '560038', '918310918812', 'napiersandra285@gmail.com', '2022-10-11 16:28:01', 'napiersandra285@gmail.com', '2022-10-11 16:28:01'),
	(33, 'Si Vega Hospital Private Limited Regal Hospital', 4, '3JMM+84J, Balaji Layout, Chokkanahalli, Bengaluru, Karnataka 560064', '560064', '918884048888', 'napiersandra285@gmail.com', '2022-10-11 16:29:04', 'napiersandra285@gmail.com', '2022-10-11 16:29:04'),
	(34, 'Gowri Home Care', 4, 'Garvebhavi playa, 42 SRVS School, 1st Main Rd, Munireddy Layout, Garvebhavi Palya, Bengaluru, Karnataka 560068', '560068', '919148757717', 'napiersandra285@gmail.com', '2022-10-11 16:30:12', 'napiersandra285@gmail.com', '2022-10-11 16:30:12'),
	(35, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:31:42', 'napiersandra285@gmail.com', '2022-10-11 16:31:42'),
	(36, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:32:08', 'napiersandra285@gmail.com', '2022-10-11 16:32:08'),
	(37, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:32:17', 'napiersandra285@gmail.com', '2022-10-11 16:32:17'),
	(38, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:32:18', 'napiersandra285@gmail.com', '2022-10-11 16:32:18'),
	(39, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:32:24', 'napiersandra285@gmail.com', '2022-10-11 16:32:24'),
	(40, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:34:34', 'napiersandra285@gmail.com', '2022-10-11 16:34:34'),
	(41, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:34:34', 'napiersandra285@gmail.com', '2022-10-11 16:34:34'),
	(42, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:34:35', 'napiersandra285@gmail.com', '2022-10-11 16:34:35'),
	(43, 'Yashomati', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-11 16:35:33', 'napiersandra285@gmail.com', '2022-10-11 16:35:33'),
	(44, 'Bhawani Multi specialty', 4, 'Ferns City Road, Ferns City, Doddanekundi, Doddanekkundi, Bengaluru, Karnataka 560048', '560048', '919480208666', 'napiersandra285@gmail.com', '2022-10-11 16:36:36', 'napiersandra285@gmail.com', '2022-10-11 16:36:36'),
	(45, 'Baptist Hospital', 4, 'Bellary Rd, Vinayakanagar, Hebbal, Bengaluru, Karnataka 560024', '560024', '919480689770', 'napiersandra285@gmail.com', '2022-10-11 16:37:24', 'napiersandra285@gmail.com', '2022-10-11 16:37:24'),
	(46, 'Mangala Raitha Bhavan', 4, '2HJM+8JR, Kundamalli, Hebbal, Bengaluru, Karnataka 560024', '560024', '919481277493', 'napiersandra285@gmail.com', '2022-10-11 16:38:12', 'napiersandra285@gmail.com', '2022-10-11 16:38:12'),
	(47, 'Sri Lakshmi Hospitals Kaggadasanapura', 4, 'No. 5,6,7, 1st Cross, Kaggadasapura Main Rd, Nagappareddy Layout, C V Raman Nagar, Bengaluru, Karnataka 560093', '560093', '919481523480', 'napiersandra285@gmail.com', '2022-10-11 16:38:59', 'napiersandra285@gmail.com', '2022-10-11 16:38:59'),
	(48, 'BBMP Yelahanka Zone', 4, '444, 5th B Main Rd, Yelahanka Satellite Town, Yelahanka New Town, Bengaluru, Karnataka 560064', '560064', '919632819836', 'napiersandra285@gmail.com', '2022-10-11 16:39:54', 'napiersandra285@gmail.com', '2022-10-11 16:39:54'),
	(49, 'Sapthagiri Medical College', 4, '15, Hesarghatta Rd, Navy Layout, Chikkasandra, Chikkabanavara, Bengaluru, Karnataka 560090', '560090', '919844141315', 'napiersandra285@gmail.com', '2022-10-11 16:40:44', 'napiersandra285@gmail.com', '2022-10-11 16:40:44'),
	(50, 'Blossom Multispeciality', 4, '98, Hosa Rd, opposite old rto office, Naganathapura, Rayasandra, Bengaluru, Karnataka 560100', '560100', '919845802557', 'napiersandra285@gmail.com', '2022-10-11 16:41:35', 'napiersandra285@gmail.com', '2022-10-11 16:41:35'),
	(51, 'Apollo Hospitals', 383, 'CIT Road', '700062', '9874897589', 'info@caregiversaathi.co.in', '2022-10-12 06:27:28', 'info@caregiversaathi.co.in', '2022-10-12 06:27:28'),
	(52, 'Apollo Hospitals', 383, '152 CIT Road', '700023', '9874897589', 'info@caregiversaathi.co.in', '2022-10-12 06:43:24', 'info@caregiversaathi.co.in', '2022-10-12 06:43:24'),
	(53, 'Ekta Hospital', 1, 'F Building, Rutu Park Service Road, Thane West, Thane - 400601, Near Sheetal Diary, Panchsheel Nagar, Devashree Garden, Majiwada', '400601', '912221721111', 'nandukrishnanka@gmail.com', '2022-10-12 08:14:27', 'nandukrishnanka@gmail.com', '2022-10-12 09:50:40'),
	(54, 'Mallige Hospital', 4, '31/32, Crescent Rd, Madhava Nagar, Gandhi Nagar, Bengaluru, Karnataka 560001', '560001', '919448077253', 'napiersandra285@gmail.com', '2022-10-12 11:54:34', 'napiersandra285@gmail.com', '2022-10-12 12:11:43'),
	(55, 'Blossom Multispecialty Hospital', 4, '98, Hosa Rd, opposite old rto office, Naganathapura, Rayasandra, Bengaluru, Karnataka 560100', '560100', '917892408186', 'napiersandra285@gmail.com', '2022-10-12 15:34:38', 'napiersandra285@gmail.com', '2022-10-12 15:34:38'),
	(56, 'Sai Thunga Multispeciality', 4, '187/269, Dr Puneeth Rajkumar Rd, Agara Village, 1st Sector, HSR Layout, Bengaluru, Karnataka 560102', '560102', '917899674400', 'napiersandra285@gmail.com', '2022-10-12 15:36:22', 'napiersandra285@gmail.com', '2022-10-12 15:36:22'),
	(57, 'Manipal Hospital ', 4, '98, HAL Old Airport Rd, Kodihalli, Bengaluru, Karnataka 560017', '560017', '918025023200', 'napiersandra285@gmail.com', '2022-10-12 15:40:19', 'napiersandra285@gmail.com', '2022-10-12 15:40:19'),
	(58, 'BGS Gleneagle Global Hospital', 4, '67, Uttarahalli Main Rd, Sunkalpalya, Bengaluru, Karnataka 560060', '560060', '918026255555', 'napiersandra285@gmail.com', '2022-10-12 15:42:12', 'napiersandra285@gmail.com', '2022-10-12 15:42:12'),
	(59, 'Republic Hospital', 4, '5, Langford Gardens, Bengaluru, Karnataka 560025', '560025', '918041245450', 'napiersandra285@gmail.com', '2022-10-12 15:43:56', 'napiersandra285@gmail.com', '2022-10-12 15:43:56'),
	(60, 'Sakra ', 4, 'SY NO 52/2 & 52/3, Devarabeesanahalli, Varthur Hobli Opp Intel, Dr Puneeth Rajkumar Rd, Marathahalli, Bengaluru, Karnataka 560103', '560103', '918049694969', 'napiersandra285@gmail.com', '2022-10-12 15:46:14', 'napiersandra285@gmail.com', '2022-10-12 15:46:14'),
	(61, 'Fortis Hospital	', 4, '23, 80 Feet Road, Guru Krupa Layout, 2nd Stage', '560072', '918023014117', 'napiersandra285@gmail.com', '2022-10-12 15:54:17', 'napiersandra285@gmail.com', '2022-10-12 15:54:17'),
	(62, 'Fortis hospital', 4, '154 9, Bannerghatta Main Rd, opposite IIM, Sahyadri Layout, Bilekahalli, Bengaluru, Karnataka 560076', '560076', '09663367253', 'napiersandra285@gmail.com', '2022-10-12 15:58:04', 'napiersandra285@gmail.com', '2022-10-12 15:58:04'),
	(63, 'Atreum Hospital	', 4, 'Ideal Homes Township, Kenchenahalli, Rajarajeshwarinagar, Bengaluru 560098', '560098', '919606980209', 'napiersandra285@gmail.com', '2022-10-12 16:21:43', 'napiersandra285@gmail.com', '2022-10-12 16:21:43'),
	(64, 'Si Vega Hospital Private Limited Regal Hospital', 4, '3JMM+84J, Balaji Layout, Chokkanahalli, Bengaluru, Karnataka 560064', '560064', '918884048888', 'napiersandra285@gmail.com', '2022-10-12 16:24:47', 'napiersandra285@gmail.com', '2022-10-12 16:24:47'),
	(65, 'Gowri Home Care', 4, 'Garvebhavi playa, 42 SRVS School, 1st Main Rd, Munireddy Layout, Garvebhavi Palya, Bengaluru, Karnataka 560068', '560068', '919148757717', 'napiersandra285@gmail.com', '2022-10-12 16:25:50', 'napiersandra285@gmail.com', '2022-10-12 16:25:50'),
	(66, 'Yashomati ', 4, '2371/3, Old HAL Airport Varthur Rd, Munnekolala, Marathahalli, Bengaluru, Karnataka 560037', '560037', '919243419712', 'napiersandra285@gmail.com', '2022-10-12 16:33:48', 'napiersandra285@gmail.com', '2022-10-12 16:33:48'),
	(67, 'Bhawani Multi specialty', 4, 'Ferns City Road, Ferns City, Doddanekundi, Doddanekkundi, Bengaluru, Karnataka 560048', '560048', '919480208666', 'napiersandra285@gmail.com', '2022-10-12 16:35:35', 'napiersandra285@gmail.com', '2022-10-12 16:35:35');

-- Dumping structure for table caregiversaathi.verified_leads
DROP TABLE IF EXISTS `verified_leads`;
CREATE TABLE IF NOT EXISTS `verified_leads` (
  `verified_lead_id` int NOT NULL AUTO_INCREMENT,
  `disease_id` int NOT NULL,
  `service_id` int NOT NULL,
  `service_attribute_id` int unsigned DEFAULT NULL,
  `vendor_id` int unsigned NOT NULL,
  `stock` varchar(50) DEFAULT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `approved_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `volunteer_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `volunteer_remarks` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `admin_remarks` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `volunteer_leads_verification_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`verified_lead_id`) USING BTREE,
  UNIQUE KEY `disease_id_service_id_service_attribute_id_vendor_id` (`disease_id`,`service_id`,`service_attribute_id`,`vendor_id`),
  KEY `disease_id` (`disease_id`),
  KEY `service_id` (`service_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `approved_by` (`approved_by`),
  KEY `volunteer_id` (`volunteer_id`),
  KEY `service_attribute_id` (`service_attribute_id`) USING BTREE,
  CONSTRAINT `FK_verified_leads_diseases` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`disease_id`),
  CONSTRAINT `FK_verified_leads_service_attributes` FOREIGN KEY (`service_attribute_id`) REFERENCES `service_attributes` (`service_attribute_id`),
  CONSTRAINT `FK_verified_leads_services` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
  CONSTRAINT `FK_verified_leads_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table caregiversaathi.verified_leads: ~13 rows (approximately)
DELETE FROM `verified_leads`;
INSERT INTO `verified_leads` (`verified_lead_id`, `disease_id`, `service_id`, `service_attribute_id`, `vendor_id`, `stock`, `create_ui`, `create_ts`, `last_update_ui`, `last_update_ts`, `approved_by`, `volunteer_id`, `volunteer_remarks`, `admin_remarks`, `volunteer_leads_verification_ts`) VALUES
	(3, 1, 1, 2, 18, '20', 'info@caregiversaathi.co.in', '2022-09-16 10:53:23', 'info@caregiversaathi.co.in', '2022-09-16 10:53:23', 'info@caregiversaathi.co.in', 'minakshy.iyer@caregiversaathi.co.in', NULL, NULL, '2022-09-16 10:53:23'),
	(4, 1, 1, 2, 19, '10', 'info@caregiversaathi.co.in', '2022-09-17 09:51:58', 'info@caregiversaathi.co.in', '2022-09-17 09:51:58', 'info@caregiversaathi.co.in', 'eknabiya@gmail.com', 'Bed number can vary ', NULL, '2022-09-17 09:51:58'),
	(5, 1, 1, 1, 19, '20', 'info@caregiversaathi.co.in', '2022-09-17 09:52:06', 'info@caregiversaathi.co.in', '2022-09-17 09:52:06', 'info@caregiversaathi.co.in', 'eknabiya@gmail.com', 'Bed number can vary ', NULL, '2022-09-17 09:52:06'),
	(6, 1, 1, 1, 18, '50', 'info@caregiversaathi.co.in', '2022-09-17 09:53:37', 'info@caregiversaathi.co.in', '2022-09-17 09:53:37', 'info@caregiversaathi.co.in', 'minakshy.iyer@caregiversaathi.co.in', NULL, NULL, '2022-09-17 09:53:37'),
	(7, 1, 2, 3, 16, '10', 'info@caregiversaathi.co.in', '2022-09-17 09:57:04', 'info@caregiversaathi.co.in', '2022-09-17 09:57:04', 'info@caregiversaathi.co.in', 'info@caregiversaathi.co.in', NULL, NULL, '2022-09-17 09:57:04'),
	(8, 1, 1, 2, 22, 'Yes', 'info@caregiversaathi.co.in', '2022-09-17 09:58:54', 'info@caregiversaathi.co.in', '2022-09-17 09:58:54', 'info@caregiversaathi.co.in', 'vimala.mv@caregiversaathi.co.in', NULL, NULL, '2022-09-17 09:58:54'),
	(9, 1, 1, 1, 22, 'Yes', 'info@caregiversaathi.co.in', '2022-09-17 10:00:02', 'info@caregiversaathi.co.in', '2022-09-17 10:00:02', 'info@caregiversaathi.co.in', 'vimala.mv@caregiversaathi.co.in', NULL, NULL, '2022-09-17 10:00:02'),
	(10, 1, 8, 15, 23, '0', 'info@caregiversaathi.co.in', '2022-09-17 10:00:38', 'info@caregiversaathi.co.in', '2022-09-17 10:00:38', 'info@caregiversaathi.co.in', 'minakshy.iyer@caregiversaathi.co.in', NULL, NULL, '2022-09-17 10:00:38'),
	(11, 2, 1, 1, 17, '10', 'info@caregiversaathi.co.in', '2022-09-17 10:43:06', 'info@caregiversaathi.co.in', '2022-09-17 10:43:06', 'info@caregiversaathi.co.in', 'info@caregiversaathi.co.in', 'Verified', NULL, '2022-09-17 10:43:06'),
	(12, 1, 1, 2, 53, '20', 'info@caregiversaathi.co.in', '2022-10-12 09:50:32', 'info@caregiversaathi.co.in', '2022-10-12 09:50:32', 'info@caregiversaathi.co.in', 'nandukrishnanka@gmail.com', 'NA', NULL, '2022-10-12 09:50:32'),
	(13, 1, 1, 1, 53, '100', 'info@caregiversaathi.co.in', '2022-10-12 09:50:40', 'info@caregiversaathi.co.in', '2022-10-12 09:50:40', 'info@caregiversaathi.co.in', 'nandukrishnanka@gmail.com', 'NA', NULL, '2022-10-12 09:50:40'),
	(14, 1, 1, 2, 54, '20', 'info@caregiversaathi.co.in', '2022-10-12 12:11:37', 'info@caregiversaathi.co.in', '2022-10-12 12:11:37', 'info@caregiversaathi.co.in', 'napiersandra285@gmail.com', 'Open 24 hours for accidents and emergencies', NULL, '2022-10-12 12:11:37'),
	(15, 1, 1, 1, 54, '125', 'info@caregiversaathi.co.in', '2022-10-12 12:11:43', 'info@caregiversaathi.co.in', '2022-10-12 12:11:43', 'info@caregiversaathi.co.in', 'napiersandra285@gmail.com', 'Open 24 hours for accidents and emergencies', NULL, '2022-10-12 12:11:43');

-- Dumping structure for table caregiversaathi.volunteer_languages_mapping
DROP TABLE IF EXISTS `volunteer_languages_mapping`;
CREATE TABLE IF NOT EXISTS `volunteer_languages_mapping` (
  `volunteer_work_area_mapping_id` int NOT NULL AUTO_INCREMENT,
  `volunteer_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `language_id` smallint unsigned NOT NULL DEFAULT '0',
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`volunteer_work_area_mapping_id`) USING BTREE,
  KEY `user_id_idx` (`volunteer_id`) USING BTREE,
  KEY `work_area_id_idx` (`language_id`) USING BTREE,
  CONSTRAINT `FK_volunteer_languages_mapping_languages` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table caregiversaathi.volunteer_languages_mapping: ~0 rows (approximately)
DELETE FROM `volunteer_languages_mapping`;

-- Dumping structure for table caregiversaathi.volunteer_work_area_mapping
DROP TABLE IF EXISTS `volunteer_work_area_mapping`;
CREATE TABLE IF NOT EXISTS `volunteer_work_area_mapping` (
  `volunteer_work_area_mapping_id` int NOT NULL AUTO_INCREMENT,
  `volunteer_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `work_area_id` int NOT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`volunteer_work_area_mapping_id`),
  KEY `user_id_idx` (`volunteer_id`),
  KEY `work_area_id_idx` (`work_area_id`),
  CONSTRAINT `FK_volunteer_work_area_mapping_work_area` FOREIGN KEY (`work_area_id`) REFERENCES `work_area` (`work_area_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.volunteer_work_area_mapping: ~0 rows (approximately)
DELETE FROM `volunteer_work_area_mapping`;

-- Dumping structure for table caregiversaathi.work_area
DROP TABLE IF EXISTS `work_area`;
CREATE TABLE IF NOT EXISTS `work_area` (
  `work_area_id` int NOT NULL AUTO_INCREMENT,
  `work_area_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'info@caregiversaathi.co.in',
  `create_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_ui` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'info@caregiversaathi.co.in',
  `last_update_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`work_area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table caregiversaathi.work_area: ~0 rows (approximately)
DELETE FROM `work_area`;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
