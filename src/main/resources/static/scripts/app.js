function populateStates(addNullValue, state, city, defaultState, defaultCity) {
	$
		.ajax({
			type: 'POST',
			url: '/ajax/getAllStates',
			data: {
			},
			success: function(result) {
				var states = JSON.parse(result);
				$('#' + state).empty();

				if (addNullValue) {
					var option = "<option value = ''>Select</option>";
					$('#' + state).append(option);
				}

				for (var i = 0; i < states.length; i++) {
					var option = "<option value = '" + states[i] + "'>"
						+ states[i] + "</option>";
					$('#' + state).append(option);
				}

				if (null != defaultState) {
					$('#' + state).val(defaultState);
				}

				if (null != city) {
					populateCities(true, state, city, defaultCity);
				}

			},
			error: function(error) {
				console
					.log(error);
			}
		});
	return false;
}


function populateCities(addNullValue, state, city, defaultCity) {
	var state = $('#' + state).val();
	if (state === "Select state") {
		alert("Select valid state value")
		$('#' + city).empty()
		$('#' + city)
			.append(
				"<option value = 'Select city'>Select city</option>");
	} else {
		$
			.ajax({
				type: 'POST',
				url: '/cities/getCitiesByState',
				data: {
					"state": state
				},
				success: function(result) {
					var cities = JSON.parse(result);
					$('#' + city).empty();

					if (addNullValue) {
						var option = "<option value = ''>Select</option>";
						$('#' + city).append(option);
					}

					for (var i = 0; i < cities.length; i++) {
						var option = "<option value = " + cities[i].city_id + " label=" + cities[i].city_name + ">"
							+ cities[i].city_name + "</option>";
						$('#' + city).append(option);
					}
					if (null != defaultCity) {
						$('#' + city).val(defaultCity);
					}
					if ('undefined' !== typeof getExisitingVendorData) {
						$('#vendor-city').on("change", function() {
							getExisitingVendorData();
						});
						getExisitingVendorData();
					}
				},
				error: function(error) {
					console
						.log(error);
				}
			});

	}
	return false;
}

function populateDiseases(disease, defaultDisease) {
	$
		.ajax({
			type: 'POST',
			url: '/ajax/getAllEnabledDiseases',
			data: {
			},
			success: function(result) {
				var diseases = JSON.parse(result);
				$('#' + disease).empty();
				for (var i = 0; i < diseases.length; i++) {
					var option = "<option value = '" + diseases[i].disease_id + "' label='" + diseases[i].disease_name + "'>"
						+ diseases[i].disease_name + "</option>";
					$('#' + disease).append(option);
				}
				if (null != defaultDisease) {
					$('#' + disease).val(defaultDisease);
				}

			},
			error: function(error) {
				console
					.log(error);
			}
		})
}


function populateAllLanguages(language) {
	$
		.ajax({
			type: 'POST',
			url: '/ajax/getAllLanguages',
			data: {
			},
			success: function(result) {
				var languages = JSON.parse(result);
				$('#' + language).empty();
				for (var i = 0; i < languages.length; i++) {
					var option = "<option value = '" + languages[i].language_id + "' label='" + languages[i].language_name + "'>"
						+ languages[i].language_name + "</option>";
					$('#' + language).append(option);
				}
				$("#" + language).multiselect({
					enableHTML: false,
					enableFiltering: false,
					numberDisplayed: 0
				});

			},
			error: function(error) {
				console
					.log(error);
			}
		})
}

function populateHours(hours) {
	for (var i = 1; i <= 24; i++) {
		var option = "<option value = '" + i + "' label='" + i + "'>"
			+ i + "</option>";
		$('#' + hours).append(option);
	}
}